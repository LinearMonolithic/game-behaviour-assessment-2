﻿using System.IO;
using System.Text.RegularExpressions;

using Kittan.General.Resources.Abstract;
using Kittan.General.Resources.Json;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Newtonsoft.Json;
using TiledSharp;

namespace Kittan.General.Resources.Concrete
{
    public class ResourceManager : IResourceManager
    {
        private ContentManager contentManager;
        private ICollisionMeshFactory collisionMeshFactory;

        public ResourceManager(ContentManager contentManager, ICollisionMeshFactory collisionMeshFactory)
        {
            this.contentManager = contentManager;
            this.collisionMeshFactory = collisionMeshFactory;
        }

        public Effect LoadEffect(string assetName)
        {
            return contentManager.Load<Effect>(assetName);
        }

        public Model LoadModel(string assetName)
        {
            return contentManager.Load<Model>(assetName);
        }

        public SoundEffect LoadSoundEffect(string assetName)
        {
            return contentManager.Load<SoundEffect>(assetName);
        }

        public SpriteFont LoadSpriteFont(string assetName)
        {
            return contentManager.Load<SpriteFont>(assetName);
        }

        public Texture2D LoadTexture2D(string assetName)
        {
            return contentManager.Load<Texture2D>(assetName);
        }

        public Texture2D LoadTextureForTileset(TmxTileset tileset)
        {
            var contentRegex = new Regex($".*/Content/");
            var tilesetAssetName = Path.ChangeExtension(contentRegex.Replace(tileset.Image.Source, string.Empty), null);

            return contentManager.Load<Texture2D>(tilesetAssetName);
        }

        public ISpritesheet LoadSpritesheet(string assetName)
        {
            var spritesheetDto = JsonConvert.DeserializeObject<SpritesheetDto>(File.ReadAllText($"{contentManager.RootDirectory}/{assetName}.json"));
            var spritesheetTexture = contentManager.Load<Texture2D>(spritesheetDto.assetName);

            return new Spritesheet(spritesheetDto, spritesheetTexture, collisionMeshFactory);
        }
    }
}
