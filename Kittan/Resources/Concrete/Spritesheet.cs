﻿using System.Collections.Generic;

using Kittan.General.Resources.Abstract;
using Kittan.General.Resources.Json;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Resources.Concrete
{
    /// <summary>
    /// This class represents and provides support function for spritesheets.
    /// </summary>
    public class Spritesheet : ISpritesheet
    {
        private ICollisionMeshFactory collisionMeshFactory;
        private Texture2D spritesheetTexture;
        private Dictionary<string, Rectangle> sprites;

        public void DrawSprite(string name, Vector2 position, SpriteBatch spriteBatch, bool direction)
        {
            var spriteRectangle = sprites[name];
            var spriteEffect = direction
                ? SpriteEffects.None
                : SpriteEffects.FlipHorizontally;
            // If direction is true, draw as normal. Otherwise, invert
            spriteBatch.Draw(
                spritesheetTexture,
                new Rectangle((int)(position.X - spriteRectangle.Width / 2), (int)(position.Y - spriteRectangle.Height / 2), spriteRectangle.Width, spriteRectangle.Height),
                spriteRectangle,
                Color.White,
                0.0f,
                new Vector2(0, 0),
                spriteEffect,
                0.0f);
        }

        /// <summary>
        /// Gets the collision mesh for the sprite. Returns a custom one if one
        /// is specified, otherwise returns a mesh that encompasses the sprite.
        /// </summary>
        public ICollisionMesh GetCollisionMeshForSprite(string name, Vector2 position)
        {
            var spriteRectangle = sprites[name];

            //TODO: Check for custom meshes in the file
            return collisionMeshFactory.CreateCollisionMesh(
                position,
                new List<Vector2>()
                {
                    new Vector2(-spriteRectangle.Width / 2, -spriteRectangle.Height / 2),
                    new Vector2(-spriteRectangle.Width / 2, spriteRectangle.Height / 2),
                    new Vector2(spriteRectangle.Width / 2, spriteRectangle.Height / 2),
                    new Vector2(spriteRectangle.Width / 2, -spriteRectangle.Height / 2)
                });
        }

        public Spritesheet(SpritesheetDto spritesheetDto, Texture2D spritesheetTexture, ICollisionMeshFactory collisionMeshFactory)
        {
            this.spritesheetTexture = spritesheetTexture;
            this.collisionMeshFactory = collisionMeshFactory;
            sprites = new Dictionary<string, Rectangle>();

            foreach (var spriteDto in spritesheetDto.sprites)
            {
                sprites.Add(
                    spriteDto.name,
                    new Rectangle(
                        spriteDto.x,
                        spriteDto.y,
                        spriteDto.width,
                        spriteDto.height));
            }
        }
    }
}
