﻿using Autofac;
using Kittan.General.Resources.Concrete;

namespace Kittan.General.Resources
{
    public class ResourcesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ResourceManager>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
