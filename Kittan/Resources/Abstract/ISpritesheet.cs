﻿using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Resources.Abstract
{
    public interface ISpritesheet
    {
        void DrawSprite(string name, Vector2 position, SpriteBatch spriteBatch, bool direction);
        ICollisionMesh GetCollisionMeshForSprite(string name, Vector2 position);
    }
}
