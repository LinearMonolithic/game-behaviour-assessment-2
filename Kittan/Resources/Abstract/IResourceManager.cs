﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

using TiledSharp;

namespace Kittan.General.Resources.Abstract
{
    public interface IResourceManager
    {
        Effect LoadEffect(string assetName);
        Model LoadModel(string assetName);
        SoundEffect LoadSoundEffect(string assetName);
        SpriteFont LoadSpriteFont(string assetName);
        Texture2D LoadTexture2D(string assetName);
        Texture2D LoadTextureForTileset(TmxTileset tileset);
        ISpritesheet LoadSpritesheet(string assetName);
    }
}
