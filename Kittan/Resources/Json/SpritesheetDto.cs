﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Kittan.General.Resources.Json
{
    public class SpritesheetDto
    {
        [JsonProperty("assetName", Required = Required.Always)]
        public string assetName;

        [JsonProperty("sprites", Required = Required.Always)]
        public List<SpriteDto> sprites;
    }
}
