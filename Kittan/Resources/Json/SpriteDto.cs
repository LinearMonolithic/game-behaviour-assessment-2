﻿using Newtonsoft.Json;

namespace Kittan.General.Resources.Json
{
    [JsonObject(MemberSerialization.OptIn)]
    public class SpriteDto
    {
        [JsonProperty("name", Required = Required.Always)]
        public string name;

        [JsonProperty("x", Required = Required.Always)]
        public int x;

        [JsonProperty("y", Required = Required.Always)]
        public int y;

        [JsonProperty("width", Required = Required.Always)]
        public int width;

        [JsonProperty("height", Required = Required.Always)]
        public int height;
    }
}
