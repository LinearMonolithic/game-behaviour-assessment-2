﻿// This code was generated using a T4 template. Changes here will be lost when the template is re-run!
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

using Kittan.General.Resources.Abstract;

namespace Kittan.General.Resources
{
	public static class ResourceManagerExtensions
	{

        // Fonts

        ///<summary>
        ///fonts\visitor_14.spritefont
        ///</summary>
        public static SpriteFont fonts_visitor_14 (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpriteFont(@"fonts\visitor_14");
        }

        ///<summary>
        ///fonts\visitor_7.spritefont
        ///</summary>
        public static SpriteFont fonts_visitor_7 (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpriteFont(@"fonts\visitor_7");
        }

        // Images

        ///<summary>
        ///Images\TestBackground.jpg
        ///</summary>
        public static Texture2D Images_TestBackground (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Images\TestBackground");
        }

        ///<summary>
        ///Images\VictoryScreen.png
        ///</summary>
        public static Texture2D Images_VictoryScreen (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Images\VictoryScreen");
        }

        ///<summary>
        ///Sprites\Enemies\Drone_Body.png
        ///</summary>
        public static Texture2D Sprites_Enemies_Drone_Body (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Enemies\Drone_Body");
        }

        ///<summary>
        ///Sprites\Enemies\Drone_Gun.png
        ///</summary>
        public static Texture2D Sprites_Enemies_Drone_Gun (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Enemies\Drone_Gun");
        }

        ///<summary>
        ///Sprites\Environment\Crate.png
        ///</summary>
        public static Texture2D Sprites_Environment_Crate (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Environment\Crate");
        }

        ///<summary>
        ///Sprites\Environment\Platform.png
        ///</summary>
        public static Texture2D Sprites_Environment_Platform (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Environment\Platform");
        }

        ///<summary>
        ///Sprites\Player\Animations\Crouch_Aim_90.png
        ///</summary>
        public static Texture2D Sprites_Player_Animations_Crouch_Aim_90 (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Player\Animations\Crouch_Aim_90");
        }

        ///<summary>
        ///Sprites\Player\Animations\Jump_Start.png
        ///</summary>
        public static Texture2D Sprites_Player_Animations_Jump_Start (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Player\Animations\Jump_Start");
        }

        ///<summary>
        ///Sprites\Player\Animations\Walk_Aim_90.png
        ///</summary>
        public static Texture2D Sprites_Player_Animations_Walk_Aim_90 (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Player\Animations\Walk_Aim_90");
        }

        ///<summary>
        ///Sprites\Player\Single\Standing.png
        ///</summary>
        public static Texture2D Sprites_Player_Single_Standing (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Player\Single\Standing");
        }

        ///<summary>
        ///Sprites\Player\Single\Standing_Aim_90.png
        ///</summary>
        public static Texture2D Sprites_Player_Single_Standing_Aim_90 (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Sprites\Player\Single\Standing_Aim_90");
        }

        ///<summary>
        ///Spritesheets\Characters_Image.png
        ///</summary>
        public static Texture2D Spritesheets_Characters_Image (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Spritesheets\Characters_Image");
        }

        ///<summary>
        ///Spritesheets\Havoc_Image.png
        ///</summary>
        public static Texture2D Spritesheets_Havoc_Image (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Spritesheets\Havoc_Image");
        }

        ///<summary>
        ///Spritesheets\Spritesheet.png
        ///</summary>
        public static Texture2D Spritesheets_Spritesheet (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Spritesheets\Spritesheet");
        }

        ///<summary>
        ///Spritesheets\Terrain.png
        ///</summary>
        public static Texture2D Spritesheets_Terrain (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Spritesheets\Terrain");
        }

        ///<summary>
        ///Spritesheets\TestSpriteSheet.png
        ///</summary>
        public static Texture2D Spritesheets_TestSpriteSheet (this IResourceManager resourceManager)
        {
            return resourceManager.LoadTexture2D(@"Spritesheets\TestSpriteSheet");
        }

        // Spritesheets

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet bin_DesktopGL_Spritesheets_Characters (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"bin\DesktopGL\Spritesheets\Characters");
        }

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet bin_DesktopGL_Spritesheets_Havoc (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"bin\DesktopGL\Spritesheets\Havoc");
        }

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet bin_Windows_Spritesheets_Characters (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"bin\Windows\Spritesheets\Characters");
        }

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet bin_Windows_Spritesheets_Havoc (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"bin\Windows\Spritesheets\Havoc");
        }

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet Spritesheets_Characters (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"Spritesheets\Characters");
        }

        ///<summary>
        ///{currentName}
        ///</summary>
        public static ISpritesheet Spritesheets_Havoc (this IResourceManager resourceManager)
        {
            return resourceManager.LoadSpritesheet(@"Spritesheets\Havoc");
        }
	}
}