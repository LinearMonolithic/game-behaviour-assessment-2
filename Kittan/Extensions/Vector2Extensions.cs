﻿using Microsoft.Xna.Framework;

namespace Kittan.General.Extensions
{
    public static class Vector2Extensions
    {
        /// <summary>
        /// Gets the normal vector, always facing 'outwards' (all geometry
        /// should be stored in clockwise fashion)
        /// </summary>
        public static Vector2 GetNormal(this Vector2 vector, bool rhs = true)
        {
            return (rhs)
                ? new Vector2(vector.Y, -vector.X)
                : new Vector2(-vector.Y, vector.X);
        }
    }
}
