﻿using System.Collections.Generic;

using Kittan.Core.Abstract;
using Kittan.General.Framework.Abstract;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Scenes
{
    public class SplashScreen //: IScene
    {
        private double duration;            // How long the splash screen will last
        private List<IGameObject> logos;    // The logos to display on the splash screen

        public SplashScreen(double duration, params IGameObject[] logos)
        {
            
        }

        public void AddObject(IGameObject newObject) { }

        public void Update() { }

        public void Render(RenderTarget2D renderTarget, Viewport viewport) { }

        public void Destroy() { }

        public bool SceneFinished(out IScene nextScene)
        {
            nextScene = null;

            return false;

        }
    }
}
