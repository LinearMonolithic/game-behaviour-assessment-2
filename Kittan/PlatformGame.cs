﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Autofac;

using Kittan.Core.Abstract;
using Kittan.General.Factories.Abstract;
using Kittan.General.Environment.Abstract;
using Kittan.General.GameObjects.Abstract;
using Kittan.General.GameObjects.Enums;
using Kittan.General.Resources;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;

namespace Kittan.General
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class PlatformGame : Game
	{
		private IContainer gameContainer;
		private KeyboardState previousKeyboardState;
		private MouseState previousMouseState;

		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		IResourceManager resourceManager;
		IPhysicsEngine physicsEngine;
		IObjectFactory objectFactory;
		IRuntimeVariables runtimeVariables;

		// TESTING
		ILevel mainLevel;
        IPlayer player;

		public PlatformGame()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			gameContainer = GameContainer.Register(this);

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// Create a new SpriteBatch, which can be used to draw textures.
			spriteBatch = gameContainer.Resolve<SpriteBatch>();

			resourceManager = gameContainer.Resolve<IResourceManager>();
			physicsEngine = gameContainer.Resolve<IPhysicsEngine>();
			objectFactory = gameContainer.Resolve<IObjectFactory>();
			runtimeVariables = gameContainer.Resolve<IRuntimeVariables>();

			// TESTING
			mainLevel = gameContainer.Resolve<ILevel>();
            var map = new TiledSharp.TmxMap("Maps/DemoMap.tmx");
            var playerPosition = mainLevel.Initialise(resourceManager.Images_TestBackground(), map);
            runtimeVariables.PlayerRespawnLocation = playerPosition;

            player = gameContainer.Resolve<IPlayer>();
            player.Initialise(playerPosition, resourceManager.Spritesheets_Characters());

            runtimeVariables.TimeDilation = 0.05f;
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// TODO: Add your update logic here
			var keyboardState = Keyboard.GetState();

            if (!runtimeVariables.GameComplete)
            {
                if (keyboardState.IsKeyDown(Keys.OemTilde) && previousKeyboardState.IsKeyUp(Keys.OemPipe))
                {
                    runtimeVariables.DebugModeEnabled = !runtimeVariables.DebugModeEnabled;
                }

                if (keyboardState.IsKeyDown(Keys.C) && previousKeyboardState.IsKeyUp(Keys.C))
                {
                    physicsEngine.Clear(ClearLevel.ObjectsOnly);
                }

                if (keyboardState.IsKeyDown(Keys.Left))
                {
                    player.MoveLeft(gameTime);
                }

                if (keyboardState.IsKeyDown(Keys.Right))
                {
                    player.MoveRight(gameTime);
                }

                if (keyboardState.IsKeyDown(Keys.Space) && previousKeyboardState.IsKeyUp(Keys.Space))
                {
                    player.Jump(gameTime);
                }
            }

			previousKeyboardState = keyboardState;

			MouseState mouseState = Mouse.GetState();

			if (mouseState.LeftButton == ButtonState.Pressed && previousMouseState.LeftButton == ButtonState.Released) {
				objectFactory.CreateObject(ObjectType.Crate, mouseState.Position.ToVector2());
			}

			previousMouseState = mouseState;

            if (!runtimeVariables.GameComplete)
			    physicsEngine.Update(gameTime);

            if (runtimeVariables.GameComplete)
            {
                physicsEngine.Clear(ClearLevel.All);
            }

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			spriteBatch.Begin();

            //TODO: Add your drawing code here
            if (!runtimeVariables.GameComplete)
            {
                mainLevel.Render();
                physicsEngine.Render();
            }
            else
            {
                spriteBatch.Draw(resourceManager.Images_VictoryScreen(), new Vector2(0, 0), Color.White);
            }

			spriteBatch.End();

			base.Draw(gameTime);
		}
	}
}
