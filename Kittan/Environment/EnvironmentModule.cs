﻿using Autofac;
using Kittan.General.Environment.Concrete;

namespace Kittan.General.Environment
{
	public class EnvironmentModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
            builder.RegisterType<EnvironmentFactory>().AsImplementedInterfaces().SingleInstance();

			builder.RegisterType<Level>().AsImplementedInterfaces();
			builder.RegisterType<MapTile>().AsImplementedInterfaces();
			builder.RegisterType<LevelCollider>().AsImplementedInterfaces();
		}
	}
}
