﻿using Kittan.AI.Pathfinding.Abstract;
using Kittan.Core.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using TiledSharp;

namespace Kittan.General.Environment.Abstract
{
	public interface ILevel : IGameObject
	{
        IGraph NavGraph { get; }

		Vector2 Initialise(Texture2D backgroundImage, TmxMap tmxMap);
	}
}
