﻿using Kittan.Core.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Environment.Abstract
{
	interface IMaptile : IGameObject
	{
		void Initialise(Vector2 position, Texture2D spriteSheet, Rectangle spriteSheetPosition, bool isCollideable);
	}
}
