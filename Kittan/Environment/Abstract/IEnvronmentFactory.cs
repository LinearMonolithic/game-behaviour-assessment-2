﻿using TiledSharp;
namespace Kittan.General.Environment.Abstract
{
	public interface IEnvironmentFactory
	{
		ILevelCollider CreateLevelCollider(TmxObject collisionObject);
	}
}
