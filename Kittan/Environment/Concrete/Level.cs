﻿using System;
using System.Collections.Generic;
using System.Linq;

using Kittan.AI.Enums;
using Kittan.AI.Pathfinding.Abstract;
using Kittan.General.Environment.Abstract;
using Kittan.General.Factories.Abstract;
using Kittan.General.GameObjects.Enums;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using TiledSharp;

namespace Kittan.General.Environment.Concrete
{
    public class Level : ILevel
    {
        private List<MapTile> map;
        private Texture2D backgroundImage;
        private SpriteBatch spriteBatch;
        private IResourceManager resourceManager;
        private IPhysicsEngine physicsEngine;
        private IEnvironmentFactory environmentFactory;
        private IObjectFactory objectFactory;
        private INPCFactory npcFactory;
        private IGraph navGraph;
        private Random random;

        public IGraph NavGraph
        {
            get { return navGraph; }
        }

        public Level(
            SpriteBatch spriteBatch,
            IResourceManager resourceManager,
            IPhysicsEngine physicsEngine,
            IEnvironmentFactory environmentFactory,
            IObjectFactory objectFactory,
            INPCFactory npcFactory,
            IGraph navGraph)
        {
            map = new List<MapTile>();
            random = new Random();

            this.spriteBatch = spriteBatch;
            this.resourceManager = resourceManager;
            this.physicsEngine = physicsEngine;
            this.environmentFactory = environmentFactory;
            this.objectFactory = objectFactory;
            this.npcFactory = npcFactory;
            this.navGraph = navGraph;
        }

        /// <summary>
        /// Loads a map from the specified file and returns the player start position
        /// </summary>
		public Vector2 Initialise(Texture2D backgroundImage, TmxMap tmxMap)
        {
            this.backgroundImage = backgroundImage;
            return ParseTmxMap(tmxMap);
        }

        public void Update(GameTime gameTime)
        {
        }

        public void Render()
        {
            spriteBatch.Draw(backgroundImage, new Vector2(0, 0), Color.White);

            foreach (var mapTile in map)
            {
                mapTile.Render();
            }
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Parses a TMX map into an array of maptile objects.
        /// Adapted from code by Temeez (https://github.com/Temeez/TiledSharp-MonoGame-Example)
        /// 
        /// Returns the player start position.
        /// </summary>
        private Vector2 ParseTmxMap(TmxMap tmxMap)
        {
            map = new List<MapTile>();
            Vector2 playerPosition = new Vector2(0, 0);

            var tileSet = tmxMap.Tilesets[0];
            var tileSetTexture = resourceManager.LoadTextureForTileset(tileSet);
            var tilesWide = tileSetTexture.Width / tileSet.TileWidth;           // How many tiles wide the tileset texture is
            var tilesHigh = tileSetTexture.Height / tileSet.TileHeight;         // How many tiles high the tileset texture is

            for (int currentTileNumber = 0; currentTileNumber < tmxMap.Layers[0].Tiles.Count; ++currentTileNumber)
            {
                var newTile = new MapTile(spriteBatch);
                int gid = tmxMap.Layers[0].Tiles[currentTileNumber].Gid;

                // If the tile isn't empty we also need to initialise it
                if (gid != 0)
                {
                    int tileFrame = gid - 1;
                    int column = tileFrame % tilesWide;
                    int row = (int)Math.Floor((double)tileFrame / (double)tilesWide);

                    var position = new Vector2(
                        (currentTileNumber % tmxMap.Width) * tmxMap.TileWidth,
                        (float)Math.Floor(currentTileNumber / (double)tmxMap.Width) * tmxMap.TileHeight);

                    var spriteSheetRectangle = new Rectangle(tileSet.TileWidth * column, tileSet.TileHeight * row, tileSet.TileWidth, tileSet.TileHeight);

                    newTile.Initialise(position, tileSetTexture, spriteSheetRectangle, false);
                }

                map.Add(newTile);
            }

            // Now, load collision meshes
            foreach (var currentCollisionObject in tmxMap.ObjectGroups["Collision Layer"].Objects)
            {
                var levelCollider = environmentFactory.CreateLevelCollider(currentCollisionObject);

                physicsEngine.AddPhysicsObject(levelCollider);
            }

            // Place objects
            foreach (var currentGameObject in tmxMap.ObjectGroups["Object Layer"].Objects)
            {
                var position = new Vector2(0, 0);
                if (Enum.TryParse(currentGameObject.Properties["Placement"], out Placement placement))
                {
                    switch (placement)
                    {
                        case Placement.Centre:
                            position = new Vector2(
                                (float)(currentGameObject.X + currentGameObject.Width / 2),
                                (float)(currentGameObject.Y + currentGameObject.Height / 2));
                            break;

                        case Placement.Random:
                            position = new Vector2(
                                (float)(currentGameObject.X + random.NextDouble() * currentGameObject.Width),
                                (float)(currentGameObject.Y + random.NextDouble() * currentGameObject.Height));
                            break;
                        case Placement.TopLeft:
                            position = new Vector2(
                                (float)currentGameObject.X,
                                (float)currentGameObject.Y);
                            break;
                    }

                    if (currentGameObject.Properties["ObjectType"] == "Player")
                    {
                        playerPosition = position;
                    }
                    else
                    {
                        if (Enum.TryParse(currentGameObject.Properties["ObjectType"], out ObjectType objectType))
                        {
                            objectFactory.CreateObject(objectType, position, currentGameObject.Properties["AdditionalArgs"]);
                        }
                    }
                }
            }

            // Load nav graph

            // Load nodes
            foreach (var currentGameObject in tmxMap.ObjectGroups["Pathfinding Layer"].Objects.Where(o => o.ObjectType == TmxObjectType.Ellipse))
            {
                if (Enum.TryParse(currentGameObject.Properties["NodeType"], out NodeType nodeType))
                {
                    // Nodes are represented as 1x1 tile elipses
                    navGraph.AddNode(
                        (int)(currentGameObject.X + currentGameObject.Width / 2),
                        (int)(currentGameObject.Y + currentGameObject.Height / 2),
                        nodeType);
                }
                else
                {
                    throw new FormatException("NodeType property in incorrect format.");
                }
            }

            // Load edges
            foreach (var currentGameObject in tmxMap.ObjectGroups["Pathfinding Layer"].Objects.Where(o => o.ObjectType == TmxObjectType.Polyline))
            {
                if (Enum.TryParse(currentGameObject.Properties["EdgeType"], out EdgeType edgeType))
                {
                    for(int i = 0; i < currentGameObject.Points.Count - 1; ++i)
                    {
                        var curPoint = currentGameObject.Points[i];
                        var nextPoint = currentGameObject.Points[i + 1];

                        navGraph.AddEdge(
                            navGraph.Nodes.Single(n => n.X == currentGameObject.X + curPoint.X && n.Y == currentGameObject.Y + curPoint.Y),
                            navGraph.Nodes.Single(n => n.X == currentGameObject.X + nextPoint.X && n.Y == currentGameObject.Y + nextPoint.Y),
                            bool.Parse(currentGameObject.Properties["Bidirectional"]),
                            edgeType
                            );
                    }
                }
            }

            // Load NPCs

            foreach (var currentGameObject in tmxMap.ObjectGroups["NPC Layer"].Objects)
            {
                var position = new Vector2(0, 0);

                if (Enum.TryParse(currentGameObject.Properties["Placement"], out Placement placement))
                {
                    switch (placement)
                    {
                        case Placement.Centre:
                            position = new Vector2(
                                (float)(currentGameObject.X + currentGameObject.Width / 2),
                                (float)(currentGameObject.Y + currentGameObject.Height / 2));
                            break;

                        case Placement.Random:
                            position = new Vector2(
                                (float)(currentGameObject.X + random.NextDouble() * currentGameObject.Width),
                                (float)(currentGameObject.Y + random.NextDouble() * currentGameObject.Height));
                            break;
                    }

                    if (Enum.TryParse(currentGameObject.Properties["NPCType"], out NPCType npcType))
                    {
                        npcFactory.CreateNPC(npcType, position, navGraph);
                    }
                }
            }

            return playerPosition;
        }
    }
}
