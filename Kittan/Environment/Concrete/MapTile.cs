﻿using Kittan.General.Environment.Abstract;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Environment.Concrete
{
	public class MapTile : IMaptile
	{
		private SpriteBatch spriteBatch;

		private Vector2 position;
		private Texture2D spriteSheet;
		private Rectangle spriteSheetPosition;

		private bool initialised;
		private bool isCollideable;

		public MapTile(SpriteBatch spriteBatch)
		{
			this.spriteBatch = spriteBatch;
			initialised = false;
		}

		/// <summary>
		/// Called to initialise the map tile
		/// </summary>
		/// <param name="x">The X position of the tile</param>
		/// <param name="y">The Y position of the tile</param>
		/// <param name="spritesheet">The sprite used to render the tile</param>
		/// <param name="isCollideable">Is the tile solid and therefore collideable?</param>
		public void Initialise(Vector2 position, Texture2D spriteSheet, Rectangle spriteSheetPosition, bool isCollideable)
		{
			this.position = position;
			this.spriteSheet = spriteSheet;
			this.spriteSheetPosition = spriteSheetPosition;
			this.spriteSheet = spriteSheet;
			this.isCollideable = isCollideable;

			initialised = true;
		}

		public void Update(GameTime gameTime)
		{
            // TODO: Could put animation support for map tiles here
		}

		public void Render()
		{
			if (initialised) {
				// TODO: add optimisation so that tiles don't draw if they're outside of the viewport
				spriteBatch.Draw(spriteSheet, position, spriteSheetPosition, Color.White);
			}
		}

		public void Destroy()
		{

		}
	}
}
