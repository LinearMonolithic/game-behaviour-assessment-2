﻿using System;

using Kittan.General.Environment.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Microsoft.Xna.Framework;

namespace Kittan.General.Environment.Concrete
{
    public class LevelCollider : ILevelCollider
    {
        private Guid physicsEngineId;
        private IRigidBody rigidBody;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.LevelCollider; }
        }

        public LevelCollider(
            IRigidBody rigidBody,
            INarrowPhaseCollisionResolver narrowPhaseCollisionResolver)
        {
            this.rigidBody = rigidBody;
            this.narrowPhaseCollisionResolver = narrowPhaseCollisionResolver;
        }

        public void Update(GameTime gameTime)
        {
            rigidBody.Update(gameTime);
        }

        public void Render()
        {
        }

        public void Destroy()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Test if a moving object collides with this level collider
        /// </summary>
        public bool CollidesWith(IPhysicsObject movingObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(movingObject, this, out collisionInfo);
        }
    }
}
