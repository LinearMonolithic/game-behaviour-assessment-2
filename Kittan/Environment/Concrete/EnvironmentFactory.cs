﻿using System.Collections.Generic;

using Kittan.General.Environment.Abstract;

using Microsoft.Xna.Framework;

using Kittan.Physics.Engine.Concrete;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.Concrete;
using Kittan.Physics.Enums;
using Kittan.Core.Abstract;

using TiledSharp;
using System.Linq;
using Kittan.Physics.Factories.Abstract;

namespace Kittan.General.Environment.Concrete
{
	public class EnvironmentFactory : IEnvironmentFactory
	{
        private IRuntimeVariables runtimeVariables;
        private ICollisionMeshFactory collisionMeshFactory;
        private INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory;
        private IRigidBodyFactory rigidBodyFactory;

		public EnvironmentFactory(
            IRuntimeVariables runtimeVariables,
            ICollisionMeshFactory collisionMeshFactory,
            INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory,
            IRigidBodyFactory rigidBodyFactory)
		{
            this.runtimeVariables = runtimeVariables;
            this.collisionMeshFactory = collisionMeshFactory;
            this.narrowPhaseCollisionResolverFactory = narrowPhaseCollisionResolverFactory;
            this.rigidBodyFactory = rigidBodyFactory;
		}

		public ILevelCollider CreateLevelCollider(TmxObject collisionObject)
		{
            List<Vector2> vertices;

            if (collisionObject.ObjectType == TmxObjectType.Basic)
            {
                vertices = new List<Vector2>()
                {
                    new Vector2(0, 0),
                    new Vector2(0, (float)collisionObject.Height),
                    new Vector2((float)collisionObject.Width, (float)collisionObject.Height),
                    new Vector2((float)collisionObject.Width, 0)
                };
            }
            else
            {
                vertices = collisionObject.Points.Select(p => new Vector2((float)p.X, (float)p.Y)).ToList();
            }

            var position = new Vector2((float)collisionObject.X, (float)collisionObject.Y);
            var collisionMesh = collisionMeshFactory.CreateCollisionMesh(position, vertices);
            var rigidBody = rigidBodyFactory.CreateRigidBody(position, false, 0, collisionMesh);
            var narrowPhaseCollisionResolver = narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(CollisionResolverType.SAT);

            return new LevelCollider(rigidBody, narrowPhaseCollisionResolver);
        }
	}
}
