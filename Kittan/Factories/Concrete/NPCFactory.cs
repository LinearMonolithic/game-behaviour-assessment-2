﻿using System;

using Kittan.AI.Pathfinding.Abstract;
using Kittan.Core.Abstract;
using Kittan.General.Factories.Abstract;
using Kittan.General.GameObjects.Enums;
using Kittan.General.GameObjects.NPCs.Concrete;
using Kittan.General.Resources;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Collision.Raytracing.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.Factories.Concrete
{
    public class NPCFactory : INPCFactory
    {
        private IPhysicsEngine physicsEngine;
        private IResourceManager resourceManager;
        private IRuntimeVariables runtimeVariables;
        private INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory;
        private IRigidBodyFactory rigidBodyFactory;
        private IRaycaster raycaster;
        private SpriteBatch spriteBatch;

        public NPCFactory(
            IPhysicsEngine physicsEngine,
            IResourceManager resourceManager,
            IRuntimeVariables runtimeVariables,
            INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory,
            IRigidBodyFactory rigidBodyFactory,
            IRaycaster raycaster,
            SpriteBatch spriteBatch)
        {
            this.physicsEngine = physicsEngine;
            this.resourceManager = resourceManager;
            this.runtimeVariables = runtimeVariables;
            this.narrowPhaseCollisionResolverFactory = narrowPhaseCollisionResolverFactory;
            this.rigidBodyFactory = rigidBodyFactory;
            this.raycaster = raycaster;
            this.spriteBatch = spriteBatch;
        }

        public Guid CreateNPC(NPCType npcType, Vector2 position, IGraph navGraph)
        {
            switch (npcType)
            {
                case NPCType.Guard:
                    var guard = new Guard(
                        spriteBatch,
                        resourceManager.Spritesheets_Characters(),
                        position,
                        runtimeVariables,
                        physicsEngine,
                        narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(Physics.Enums.CollisionResolverType.SAT),
                        rigidBodyFactory,
                        raycaster,
                        navGraph
                        );
                    return physicsEngine.AddPhysicsObject(guard);
            }

            throw new NotImplementedException("The requested NPC type does not have a factory method set up to handle it");
        }
    }
}
