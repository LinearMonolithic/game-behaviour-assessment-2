﻿using System;

using Kittan.Core.Abstract;
using Kittan.General.Factories.Abstract;
using Kittan.General.GameObjects.Concrete;
using Kittan.General.GameObjects.Enums;
using Kittan.General.GameObjects.Json;
using Kittan.General.Resources;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Newtonsoft.Json;

namespace Kittan.General.Factories.Concrete
{
    public class ObjectFactory : IObjectFactory
    {
        private IPhysicsEngine physicsEngine;
        private IResourceManager resourceManager;
        private IRuntimeVariables runtimeVariables;
        private ICollisionMeshFactory collisionMeshFactory;
        private INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory;
        private IRigidBodyFactory rigidBodyFactory;

        private SpriteBatch spriteBatch;

        public ObjectFactory(
            IPhysicsEngine physicsEngine,
            IResourceManager resourceManager,
            IRuntimeVariables runtimeVariables,
            ICollisionMeshFactory collisionMeshFactory,
            INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory,
            IRigidBodyFactory rigidBodyFactory,
            SpriteBatch spriteBatch)
        {
            this.physicsEngine = physicsEngine;
            this.resourceManager = resourceManager;
            this.runtimeVariables = runtimeVariables;
            this.collisionMeshFactory = collisionMeshFactory;
            this.narrowPhaseCollisionResolverFactory = narrowPhaseCollisionResolverFactory;
            this.rigidBodyFactory = rigidBodyFactory;
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Creates the requested game object at the specified position, adds it to the physics engine and returns its Guid.
        /// 
        /// Game objects can also have custom properties contained within a JSON string.
        /// </summary>
        public Guid CreateObject(ObjectType objectType, Vector2 position, string jsonArgs = null)
        {
            switch (objectType)
            {
                case ObjectType.Crate:
                    var crate = new Crate(
                        spriteBatch,
                        resourceManager.Sprites_Environment_Crate(),
                        position,
                        runtimeVariables,
                        narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(CollisionResolverType.SAT),
                        collisionMeshFactory, 
                        rigidBodyFactory);
                    return physicsEngine.AddPhysicsObject(crate);

                case ObjectType.MovingPlatform:
                    if (jsonArgs == null)
                        throw new ArgumentNullException("jsonArgs", "A moving platform object requires json arguments for instantiation.");
                    var movingPlatformDto = JsonConvert.DeserializeObject<MovingPlatformDto>(jsonArgs);
                    if (Enum.TryParse(movingPlatformDto.direction, out Direction direction))
                    {
                        var movingPlatform = new MovingPlatform(
                            spriteBatch,
                            resourceManager.Sprites_Environment_Platform(),
                            position,
                            movingPlatformDto.platformWidth,
                            direction,
                            movingPlatformDto.minDisplacement,
                            movingPlatformDto.maxDisplacement,
                            runtimeVariables,
                            narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(CollisionResolverType.SAT),
                            collisionMeshFactory,
                            rigidBodyFactory);
                        return physicsEngine.AddPhysicsObject(movingPlatform);
                    }
                    throw new FormatException("Direction property of moving platform object is in an incorrect format");
                case ObjectType.EndZone:
                    if (jsonArgs == null)
                        throw new ArgumentNullException("jsonArgs", "An end zone object requires json arguments for instantiation.");
                    var endzoneDto = JsonConvert.DeserializeObject<EndZoneDto>(jsonArgs);
                    var endzone = new EndZone(
                        position,
                        endzoneDto.width,
                        endzoneDto.height,
                        spriteBatch,
                        runtimeVariables,
                        narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(CollisionResolverType.SAT),
                        collisionMeshFactory,
                        rigidBodyFactory);
                    return physicsEngine.AddPhysicsObject(endzone);

            }

            throw new NotImplementedException("The requested type does not have a factory method set up to handle it");
        }
    }
}
