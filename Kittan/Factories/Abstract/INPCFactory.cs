﻿using System;

using Kittan.AI.Pathfinding.Abstract;
using Kittan.General.GameObjects.Enums;

using Microsoft.Xna.Framework;

namespace Kittan.General.Factories.Abstract
{
    public interface INPCFactory
    {
        Guid CreateNPC(NPCType npcType, Vector2 position, IGraph navGraph);
    }
}
