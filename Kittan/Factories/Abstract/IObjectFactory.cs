﻿using System;

using Kittan.General.GameObjects.Enums;

using Microsoft.Xna.Framework;

namespace Kittan.General.Factories.Abstract
{
    public interface IObjectFactory
    {
        Guid CreateObject(ObjectType objectType, Vector2 position, string jsonArgs = null);
    }
}
