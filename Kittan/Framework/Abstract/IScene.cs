﻿using Kittan.Core.Abstract;

namespace Kittan.General.Framework.Abstract
{
    public interface IScene : IGameObject
    {
        void AddObject(IGameObject newObject);
        bool SceneFinished(out IScene nextScene);
    }
}
