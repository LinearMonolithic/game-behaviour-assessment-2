﻿using System;
using System.Collections.Generic;

using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.GameObjects.Concrete
{
    class Crate : IPhysicsObject
    {
        private Guid physicsEngineId;
        private IRigidBody rigidBody;
        private IRuntimeVariables runtimeVariables;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;
        private SpriteBatch spriteBatch;
        private Texture2D sprite;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.Object; }
        }

        public Crate(
            SpriteBatch spriteBatch,
            Texture2D sprite,
            Vector2 position,
            IRuntimeVariables runtimeVariables,
            INarrowPhaseCollisionResolver narrowPhaseCollisionResolver,
            ICollisionMeshFactory collisionMeshFactory,
            IRigidBodyFactory rigidBodyFactory)
        {
            this.spriteBatch = spriteBatch;
            this.sprite = sprite;
            this.runtimeVariables = runtimeVariables;
            this.narrowPhaseCollisionResolver = narrowPhaseCollisionResolver;

            var vertices = new List<Vector2>()
            {
                new Vector2(-sprite.Width / 2, -sprite.Height / 2),
                new Vector2(-sprite.Width / 2, sprite.Height / 2),
                new Vector2(sprite.Width / 2, sprite.Height / 2),
                new Vector2(sprite.Width / 2, -sprite.Height / 2)
            };

            var collisionMesh = collisionMeshFactory.CreateCollisionMesh(position, vertices);
            rigidBody = rigidBodyFactory.CreateRigidBody(position, true, 10, collisionMesh);
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            //TODO: Account for rotation
            spriteBatch.Draw(
                sprite, 
                new Rectangle(
                    (int)(rigidBody.Position.X - sprite.Width / 2),
                    (int)(rigidBody.Position.Y - sprite.Height / 2),
                    sprite.Width,
                    sprite.Height),
                Color.White);

            if (runtimeVariables.DebugModeEnabled)
            {
                rigidBody.AxisAlignedBoundingBox.DrawForDebug(spriteBatch, runtimeVariables.DebugTexture);
            }
        }

        public void Update(GameTime gameTime)
        {
            rigidBody.Update(gameTime);
        }

        /// <summary>
        /// Treating this object as the moving object, test if it collides with
        /// the stationary object. If so, true will be returned and the
        /// collision info will be stored.
        /// </summary>
        public bool CollidesWith(IPhysicsObject stationaryObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(this, stationaryObject, out collisionInfo);
        }
    }
}
