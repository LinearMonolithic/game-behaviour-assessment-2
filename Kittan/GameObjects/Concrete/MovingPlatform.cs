﻿using System;
using System.Collections.Generic;

using Kittan.Core.Abstract;
using Kittan.General.GameObjects.Enums;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.GameObjects.Concrete
{
    class MovingPlatform : IPhysicsObject
    {
        private Guid physicsEngineId;
        private IRigidBody rigidBody;
        private IRuntimeVariables runtimeVariables;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;
        private SpriteBatch spriteBatch;
        private Texture2D sprite;

        private Direction currentDirection;
        private int platformWidth;
        private int platformSpriteWidth;
        private Vector2 minPosition;
        private Vector2 maxPosition;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.LevelCollider; }
        }

        /// <summary>
        /// Create a new moving platform. Note that the sprite for the moving
        /// platform is expected to contain three sprites: one for the left,
        /// middle and right of the platform respectively.
        /// </summary>
        public MovingPlatform(
            SpriteBatch spriteBatch,
            Texture2D sprite,
            Vector2 position,
            int platformWidth,
            Direction direction,
            int minDisplacement,
            int maxDisplacement,
            IRuntimeVariables runtimeVariables,
            INarrowPhaseCollisionResolver narrowPhaseCollisionResolver,
            ICollisionMeshFactory collisionMeshFactory,
            IRigidBodyFactory rigidBodyFactory)
        {
            this.spriteBatch = spriteBatch;
            this.sprite = sprite;
            this.runtimeVariables = runtimeVariables;
            this.narrowPhaseCollisionResolver = narrowPhaseCollisionResolver;
            this.platformWidth = platformWidth;
            this.currentDirection = direction;

            platformSpriteWidth = (sprite.Width / 3);
            var totalPlatformWidth = platformWidth * platformSpriteWidth;

            var vertices = new List<Vector2>()
            {
                new Vector2(-totalPlatformWidth / 2, -sprite.Height / 2),
                new Vector2(-totalPlatformWidth / 2, sprite.Height / 2),
                new Vector2(totalPlatformWidth / 2, sprite.Height / 2),
                new Vector2(totalPlatformWidth / 2, -sprite.Height / 2)
            };

            var collisionMesh = collisionMeshFactory.CreateCollisionMesh(position, vertices);
            rigidBody = rigidBodyFactory.CreateRigidBody(position, true, 0, collisionMesh);

            if (direction == Direction.Left || direction == Direction.Right)
            {
                minPosition = new Vector2(position.X - platformSpriteWidth * minDisplacement, position.Y);
                maxPosition = new Vector2(position.X + platformSpriteWidth * maxDisplacement, position.Y);
                rigidBody.Velocity = direction == Direction.Left
                    ? new Vector2(-0.5f, 0)
                    : new Vector2(0.5f, 0);
            }
            else
            {
                minPosition = new Vector2(position.X, position.Y - sprite.Height * minDisplacement);
                maxPosition = new Vector2(position.X, position.Y + sprite.Height * maxDisplacement);
                rigidBody.Velocity = direction == Direction.Up
                    ? new Vector2(0, -0.5f)
                    : new Vector2(0, 0.5f);
            }
        }

        public bool CollidesWith(IPhysicsObject movingObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(movingObject, this, out collisionInfo);
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            var currentRenderPosition = new Vector2(
                    rigidBody.Position.X - ((platformSpriteWidth * platformWidth) / 2),
                    rigidBody.Position.Y - sprite.Height / 2);

            // Draw the left component
            spriteBatch.Draw(
                sprite,
                currentRenderPosition,
                new Rectangle(0, 0, sprite.Width / 3, sprite.Height),
                Color.White);

            currentRenderPosition.X += platformSpriteWidth;

            // Draw the middle components
            for (int i = 0; i < platformWidth - 2; ++i)
            {
                spriteBatch.Draw(
                    sprite,
                    currentRenderPosition,
                    new Rectangle(platformSpriteWidth, 0, sprite.Width / 3, sprite.Height),
                    Color.White);

                currentRenderPosition.X += platformSpriteWidth;
            }

            // Draw the right component
            spriteBatch.Draw(
                sprite,
                currentRenderPosition,
                new Rectangle(2 * platformSpriteWidth, 0, sprite.Width / 3, sprite.Height),
                Color.White);
        }

        public void Update(GameTime gameTime)
        {
            if (currentDirection == Direction.Left || currentDirection == Direction.Right)
            {
                // Platform is moving left to right. Do a sweep test of sorts and update position
                if (currentDirection == Direction.Left && rigidBody.GetPositionAfterThisUpdate(gameTime).X <= minPosition.X)
                {
                    rigidBody.Position = minPosition;
                    rigidBody.Velocity = new Vector2(0.5f, 0);
                    currentDirection = Direction.Right;
                }
                else if (currentDirection == Direction.Right && rigidBody.GetPositionAfterThisUpdate(gameTime).X >= maxPosition.X)
                {
                    rigidBody.Position = maxPosition;
                    rigidBody.Velocity = new Vector2(-0.5f, 0);
                    currentDirection = Direction.Left;
                }
            }
            else
            {
                if (currentDirection == Direction.Up && rigidBody.GetPositionAfterThisUpdate(gameTime).Y <= minPosition.Y)
                {
                    rigidBody.Position = minPosition;
                    rigidBody.Velocity = new Vector2(0, 0.5f);
                    currentDirection = Direction.Down;
                }
                else if (currentDirection == Direction.Down && rigidBody.GetPositionAfterThisUpdate(gameTime).Y >= maxPosition.Y)
                {
                    rigidBody.Position = maxPosition;
                    rigidBody.Velocity = new Vector2(0, -0.5f);
                    currentDirection = Direction.Up;
                }
            }

            rigidBody.Update(gameTime);
        }
    }
}
