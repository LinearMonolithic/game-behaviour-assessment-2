﻿using System;

using Kittan.Core.Abstract;
using Kittan.General.GameObjects.Abstract;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.GameObjects.Concrete
{
    public class Player : IPlayer
    {
        private ISpritesheet spritesheet;
        private string currentSprite;
        private bool initialised;

        private float maxSpeed;
        private bool movingLeft;
        private bool movingRight;
        private bool onGround;

        private SpriteBatch spriteBatch;
        private IPhysicsEngine physicsEngine;
        private IRigidBody rigidBody;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;
        private INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory;
        private IRigidBodyFactory rigidBodyFactory;
        private ICollisionMeshFactory collisionMeshFactory;
        private IRuntimeVariables runtimeVariables;
        private Guid physicsEngineId;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.Player; }
        }

        public Player(
            SpriteBatch spriteBatch,
            IPhysicsEngine physicsEngine,
            INarrowPhaseCollisionResolverFactory narrowPhaseCollisionResolverFactory,
            IRigidBodyFactory rigidBodyFactory,
            ICollisionMeshFactory collisionMeshFactory,
            IRuntimeVariables runtimeVariables)
        {
            this.spriteBatch = spriteBatch;
            this.physicsEngine = physicsEngine;
            this.narrowPhaseCollisionResolverFactory = narrowPhaseCollisionResolverFactory;
            this.rigidBodyFactory = rigidBodyFactory;
            this.collisionMeshFactory = collisionMeshFactory;
            this.runtimeVariables = runtimeVariables;

            maxSpeed = 15;
            movingRight = true;
            movingLeft = false;
            onGround = false;
            initialised = false;
        }

        public void Update(GameTime gameTime)
        {
            if (initialised)
            {
                rigidBody.Update(gameTime);
                if (rigidBody.Velocity.X > maxSpeed)
                    rigidBody.Velocity = new Vector2(maxSpeed, rigidBody.Velocity.Y);
                runtimeVariables.PlayerPosition = rigidBody.Position;
            }
        }

        public void Render()
        {
            if (initialised)
            {
                spritesheet.DrawSprite(currentSprite, rigidBody.Position, spriteBatch, movingRight);

                if (runtimeVariables.DebugModeEnabled)
                {
                    rigidBody.AxisAlignedBoundingBox.DrawForDebug(spriteBatch, runtimeVariables.DebugTexture);
                }
            }
        }

        public void Destroy()
        {
            //physicsEngine.RemovePhysicsObject(physId);
        }

        public void Initialise(Vector2 position, ISpritesheet spritesheet)
        {
            this.spritesheet = spritesheet;
            currentSprite = "Standing";

            var collisionMesh = spritesheet.GetCollisionMeshForSprite(currentSprite, position);
            rigidBody = rigidBodyFactory.CreateRigidBody(position, true, 10, collisionMesh);
            rigidBody.Friction = 10.0f;
            narrowPhaseCollisionResolver = narrowPhaseCollisionResolverFactory.CreateNarrowPhaseCollisionResolver(CollisionResolverType.SAT);
            physicsEngineId = physicsEngine.AddPhysicsObject(this);

            initialised = true;
        }

        public bool CollidesWith(IPhysicsObject stationaryObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(this, stationaryObject, out collisionInfo);
        }

        public void MoveLeft(GameTime gameTime)
        {
            movingRight = false;
            movingLeft = true;

            rigidBody.AddForce(new Vector2(-50, 0), ForceMode.Force, gameTime);
        }

        public void MoveRight(GameTime gameTime)
        {
            movingLeft = false;
            movingRight = true;

            rigidBody.AddForce(new Vector2(50, 0), ForceMode.Force, gameTime);
        }

        public void Jump(GameTime gameTime)
        {
            if (physicsEngine.IsObjectTouchingTheFloor(physicsEngineId))
                rigidBody.AddForce(new Vector2(0, -2.5f), ForceMode.Impulse, gameTime);
        }
    }
}
