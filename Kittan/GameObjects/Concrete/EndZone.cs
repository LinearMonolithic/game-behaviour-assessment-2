﻿using System;
using System.Collections.Generic;

using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.GameObjects.Concrete
{
    public class EndZone : IPhysicsObject
    {
        private Guid physicsEngineId;
        private IRigidBody rigidBody;
        private SpriteBatch spriteBatch;
        private IRuntimeVariables runtimeVariables;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.EndZone; }
        }

        public EndZone(
            Vector2 position,
            float width,
            float height,
            SpriteBatch spriteBatch,
            IRuntimeVariables runtimeVariables,
            INarrowPhaseCollisionResolver narrowPhaseCollisionResolver,
            ICollisionMeshFactory collisionMeshFactory,
            IRigidBodyFactory rigidBodyFactory)
        {
            this.spriteBatch = spriteBatch;
            this.runtimeVariables = runtimeVariables;
            this.narrowPhaseCollisionResolver = narrowPhaseCollisionResolver;

            var vertices = new List<Vector2>()
            {
                new Vector2(0, 0),
                new Vector2(0, height),
                new Vector2(width, height),
                new Vector2(width, 0)
            };

            var collisionMesh = collisionMeshFactory.CreateCollisionMesh(position, vertices);
            rigidBody = rigidBodyFactory.CreateRigidBody(position, true, 10, collisionMesh);
        }

        public bool CollidesWith(IPhysicsObject movingObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(movingObject, this, out collisionInfo);
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            if (runtimeVariables.DebugModeEnabled)
            {
                rigidBody.AxisAlignedBoundingBox.DrawForDebug(spriteBatch, runtimeVariables.DebugTexture);
            }
        }

        public void Update(GameTime gameTime)
        {
            rigidBody.Update(gameTime);
        }
    }
}
