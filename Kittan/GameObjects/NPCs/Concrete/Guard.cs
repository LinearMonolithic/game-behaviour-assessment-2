﻿using System;
using System.Collections.Generic;

using Kittan.AI.Pathfinding.Abstract;
using Kittan.Core.Abstract;
using Kittan.General.Resources.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.Raytracing.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;
using Kittan.AI.Enums;

namespace Kittan.General.GameObjects.NPCs.Concrete
{
    public class Guard : IPhysicsObject
    {
        private ISpritesheet spritesheet;
        private string currentSprite;

        private bool movingLeft;
        private bool movingRight;
        private bool onGround;
        private bool canSeePlayer;
        private bool pursuingPlayer;
        private float jumpCooldown; // To prevent the AI jumping multiple times in very quick succession

        private IGraph navGraph;
        private INode currentNode;
        private INode currentTargetNode;
        private INode currentGoalNode;
        private List<IEdge> currentPath;
        private List<INode> nodesToPatrol;
        private float timeSinceLastNodeUpdate;
        private int currentGoalNodeIndex;

        private SpriteBatch spriteBatch;
        private IPhysicsEngine physicsEngine;
        private INarrowPhaseCollisionResolver narrowPhaseCollisionResolver;
        private IRuntimeVariables runtimeVariables;
        private Guid physicsEngineId;
        private IRigidBody rigidBody;
        private IRaycaster raycaster;

        public Guid PhysicsEngineId
        {
            get { return physicsEngineId; }
            set { physicsEngineId = value; }
        }

        public IRigidBody RigidBody
        {
            get { return rigidBody; }
            set { rigidBody = value; }
        }

        public PhysicsObjectType ObjectType
        {
            get { return PhysicsObjectType.NPC; }
        }

        public Guard(
            SpriteBatch spriteBatch,
            ISpritesheet spritesheet,
            Vector2 position,
            IRuntimeVariables runtimeVariables,
            IPhysicsEngine physicsEngine,
            INarrowPhaseCollisionResolver narrowPhaseCollisionResolver,
            IRigidBodyFactory rigidBodyFactory,
            IRaycaster raycaster,
            IGraph navGraph)
        {
            this.spriteBatch = spriteBatch;
            this.spritesheet = spritesheet;
            currentSprite = "Guard_Standing";

            this.runtimeVariables = runtimeVariables;
            this.physicsEngine = physicsEngine;
            this.narrowPhaseCollisionResolver = narrowPhaseCollisionResolver;

            var collisionMesh = spritesheet.GetCollisionMeshForSprite(currentSprite, position);
            rigidBody = rigidBodyFactory.CreateRigidBody(position, true, 10, collisionMesh);

            this.raycaster = raycaster;

            // Pathfinding
            this.navGraph = navGraph;
            timeSinceLastNodeUpdate = 0;    // This is used to track the time since the last current/target node change.

            // Store patrol nodes
            nodesToPatrol = navGraph.Nodes.Where((n) => n.NodeType == NodeType.Patrol).ToList();

            currentNode = navGraph.GetClosestNodeToPoint(position);
            currentGoalNode = nodesToPatrol.First();
            currentGoalNodeIndex = 0;

            currentPath = navGraph.FindPath(
                currentNode,
                currentGoalNode);

            currentTargetNode = currentPath.Single((e) => e.ParentNode == currentNode).ChildNode;
        }

        public bool CollidesWith(IPhysicsObject stationaryObject, out ICollisionInfo collisionInfo)
        {
            collisionInfo = null;
            return narrowPhaseCollisionResolver.TestCollision(this, stationaryObject, out collisionInfo);
        }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public void Render()
        {
            spritesheet.DrawSprite(currentSprite, rigidBody.Position, spriteBatch, movingRight);

            if (runtimeVariables.DebugModeEnabled)
            {
                rigidBody.AxisAlignedBoundingBox.DrawForDebug(spriteBatch, runtimeVariables.DebugTexture);
            }
        }

        public void Update(GameTime gameTime)
        {
            var closestNode = navGraph.GetClosestNodeToPoint(rigidBody.Position);

            timeSinceLastNodeUpdate += gameTime.ElapsedGameTime.Milliseconds;

            // Update jump cooldown
            if (jumpCooldown > 0)
                jumpCooldown -= gameTime.ElapsedGameTime.Milliseconds;

            if (timeSinceLastNodeUpdate >= runtimeVariables.MaxTimeBetweenNodes)
            {
                //If it's been 3 seconds and we've not reached the target, assume somethings gone wrong and re-route.
                currentNode = closestNode;
                if (currentNode == currentGoalNode)
                {
                    currentNode = currentPath.Single((e) => e.ChildNode == currentGoalNode).ParentNode;
                }
                else
                {
                    if (currentTargetNode == currentGoalNode)
                    {
                        // We can't reach the goal. Find another route.
                        Reroute(gameTime);
                        return;
                    }
                    else
                    {
                        if (currentPath.Any((e) => e.ParentNode == currentNode))
                        {
                            currentTargetNode = currentPath.Single((e) => e.ParentNode == currentNode).ChildNode;
                        }
                        else
                        {
                            // The closest node isn't even in the path. We're a long way from home, just re-route at this point.
                            //  This hardly happens but it happens enough to warrant catching this edge case.
                            Reroute(gameTime);
                            return;
                        }
                    }
                }
                timeSinceLastNodeUpdate = 0;
            }

            // If the guard can see the player, stop patrolling and target them instead.
            if (runtimeVariables.PlayerPosition.HasValue)
            {
                canSeePlayer = raycaster.TestRay(rigidBody.Position, runtimeVariables.PlayerPosition.Value, physicsEngineId);

                if (canSeePlayer)
                {
                    pursuingPlayer = true;
                    var closestNodeToPlayer = navGraph.GetClosestNodeToPoint(runtimeVariables.PlayerPosition.Value);
                    if (closestNode == closestNodeToPlayer)
                    {
                        currentTargetNode = navGraph.CreateTemporaryNode(runtimeVariables.PlayerPosition.Value);
                    }
                    else
                    {
                        if (currentGoalNode != closestNodeToPlayer)
                        {
                            currentPath = navGraph.FindPath(closestNode, closestNodeToPlayer);
                            currentGoalNode = closestNodeToPlayer;
                            currentTargetNode = currentPath.Single((e) => e.ParentNode == closestNode).ChildNode;
                            timeSinceLastNodeUpdate = 0;
                            rigidBody.Update(gameTime);
                            return;
                        }
                    }
                }
            }

            // Move to the next node in the current path
            if (currentPath != null)
            {
                // If we've reached the current target node, update the current node
                if (physicsEngine.CheckIfPointCollidesWithObject(new Vector2(currentTargetNode.X, currentTargetNode.Y), physicsEngineId))
                {
                    currentNode = currentTargetNode;
                    timeSinceLastNodeUpdate = 0;

                    if (currentNode == currentGoalNode)
                    {
                        // We've reached the final node. Time to make a new path
                        pursuingPlayer = false;
                        Reroute(gameTime);
                        return;
                    }
                    else
                    {
                        if (pursuingPlayer)
                        {
                            // The target node was a temporary node.
                            if (canSeePlayer)
                            {
                                currentTargetNode = navGraph.CreateTemporaryNode(runtimeVariables.PlayerPosition.Value);
                            }
                            else
                            {
                                Reroute(gameTime);
                                return;
                            }
                        }
                        else
                        {
                            currentTargetNode = currentPath.Single((e) => e.ParentNode == currentNode).ChildNode;
                        }
                    }
                }

                // If we've reached a node in the path that's closer than our current target (can happen if we jump over a node in the middle)
                //  then update the current node and current target node accordingly.
                if (physicsEngine.CheckIfPointCollidesWithObject(new Vector2(closestNode.X, closestNode.Y), PhysicsEngineId))
                {
                    if (currentPath.FindIndex((e) => e.ParentNode == closestNode) > currentPath.FindIndex((e) => e.ParentNode == currentNode))
                    {
                        currentNode = closestNode;
                        currentTargetNode = currentPath.Single((e) => e.ParentNode == currentNode).ChildNode;
                        timeSinceLastNodeUpdate = 0;
                    }
                }

                var edgeType = EdgeType.Jump;

                if (currentPath.Any((e) => e.ParentNode == currentNode && e.ChildNode == currentTargetNode))
                {
                    edgeType = currentPath.Single((e) => e.ParentNode == currentNode).EdgeType;
                }

                // TODO: Check is currently hardcoded since all tiles are 16x16. Change this to use a runtime variable.
                if (edgeType == EdgeType.WaitForPlatform && !physicsEngine.CheckIfPointHasFloorBeneathIt(new Vector2(currentTargetNode.X, currentTargetNode.Y + 8)) ||
                    edgeType == EdgeType.Wait)
                {
                }
                else
                {
                    if (rigidBody.Position.X < currentTargetNode.X)
                    {
                        MoveRight(gameTime);
                    }
                    else
                    {
                        MoveLeft(gameTime);
                    }

                    if (rigidBody.Position.Y > currentTargetNode.Y && edgeType != EdgeType.Walk)
                    {
                        Jump(gameTime);
                    }
                }

                rigidBody.Update(gameTime);
            }
        }

        private void Reroute(GameTime gameTime)
        {
            var closestNode = navGraph.GetClosestNodeToPoint(rigidBody.Position);
            timeSinceLastNodeUpdate = 0;
            pursuingPlayer = false;

            currentGoalNodeIndex = currentGoalNodeIndex < nodesToPatrol.Count - 1
                ? currentGoalNodeIndex + 1
                : 0;
            currentGoalNode = nodesToPatrol[currentGoalNodeIndex];
            currentPath = navGraph.FindPath(closestNode, currentGoalNode);
            currentNode = closestNode;
            currentTargetNode = currentPath.Single((e) => e.ParentNode == currentNode).ChildNode;
            rigidBody.Update(gameTime);
            return;
        }

        private void MoveLeft(GameTime gameTime)
        {
            movingRight = false;
            movingLeft = true;

            rigidBody.AddForce(new Vector2(-50, 0), ForceMode.Force, gameTime);
        }

        private void MoveRight(GameTime gameTime)
        {
            movingLeft = false;
            movingRight = true;

            rigidBody.AddForce(new Vector2(50, 0), ForceMode.Force, gameTime);
        }

        private void Jump(GameTime gameTime)
        {
            if (jumpCooldown <= 0)
            {
                if (physicsEngine.IsObjectTouchingTheFloor(physicsEngineId))
                {
                    // Sometimes the AI will jump too quickly, just after falling. To compensate, ensure we are not falling too fast.
                    if (rigidBody.Velocity.Y < 0.05f)
                    {
                        rigidBody.AddForce(new Vector2(0, -2.5f), ForceMode.Impulse, gameTime);
                        jumpCooldown = 100; // 100 millisecond cooldown
                    }
                }
            }
        }
    }
}
