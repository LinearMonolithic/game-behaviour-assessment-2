﻿namespace Kittan.General.GameObjects.Enums
{
    public enum Placement
    {
        Centre,
        Random,
        TopLeft
    }
}
