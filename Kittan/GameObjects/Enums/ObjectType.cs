﻿namespace Kittan.General.GameObjects.Enums
{
    public enum ObjectType
    {
        Crate,
        MovingPlatform,
        EndZone
    }
}
