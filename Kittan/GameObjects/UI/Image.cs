﻿using Kittan.Core.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.General.GameObjects.UI
{
    /// <summary>
    /// This class represents a simple image for the UI
    /// </summary>
    class Image //: IGameObject
    {
        private Texture2D texture;
        private Vector2 position;
        private Rectangle renderRectangle;

        public Image(Texture2D texture)
        {
            this.texture = texture;
            position = new Vector2();
            renderRectangle = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
        }

        public Image(Texture2D texture, float x, float y)
        {
            this.texture = texture;
            position = new Vector2(x, y);
        }

        public void Update() { }

        public void Render(RenderTarget2D renderTarget, Viewport viewport)
        {

        }

        public void Destroy() { }
    }
}
