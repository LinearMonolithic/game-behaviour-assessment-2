﻿using Autofac;
using Kittan.General.Factories.Concrete;
using Kittan.General.GameObjects.Concrete;

namespace Kittan.General.GameObjects
{
    public class GameObjectModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ObjectFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NPCFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<Player>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
