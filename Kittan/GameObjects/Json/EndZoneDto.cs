﻿using Newtonsoft.Json;

namespace Kittan.General.GameObjects.Json
{
    public class EndZoneDto
    {
        [JsonProperty("width", Required = Required.Always)]
        public float width;

        [JsonProperty("height", Required = Required.Always)]
        public float height;
    }
}
