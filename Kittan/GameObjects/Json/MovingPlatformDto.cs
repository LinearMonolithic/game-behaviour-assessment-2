﻿using Newtonsoft.Json;

namespace Kittan.General.GameObjects.Json
{
    public class MovingPlatformDto
    {
        [JsonProperty("platformWidth", Required = Required.Always)]
        public int platformWidth;

        [JsonProperty("direction", Required = Required.Always)]
        public string direction;

        [JsonProperty("minDisplacement", Required = Required.Always)]
        public int minDisplacement;

        [JsonProperty("maxDisplacement", Required = Required.Always)]
        public int maxDisplacement;
    }
}
