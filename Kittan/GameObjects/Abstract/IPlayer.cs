﻿using Kittan.General.Resources.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.General.GameObjects.Abstract
{
	public interface IPlayer : IPhysicsObject
	{
		void Initialise(Vector2 position, ISpritesheet spritesheet);
        void MoveLeft(GameTime gameTime);
        void MoveRight(GameTime gameTime);
        void Jump(GameTime gameTime);
	}
}
