﻿using Autofac;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Kittan.AI.Autofac;
using Kittan.Core;
using Kittan.General.Environment;
using Kittan.General.GameObjects;
using Kittan.General.Resources;
using Kittan.Physics.Autofac;

namespace Kittan.General
{
    public static class GameContainer
    {
        public static IContainer Register(Game game)
        {
            var builder = new ContainerBuilder();

            // Register instances
            builder.RegisterInstance(new SpriteBatch(game.GraphicsDevice)).AsSelf();
            builder.RegisterInstance(game.Content).AsSelf();
            builder.RegisterInstance(game.GraphicsDevice).AsSelf();

            // Register modules
            builder.RegisterModule(new EnvironmentModule());
            builder.RegisterModule(new GameObjectModule());
            builder.RegisterModule(new ResourcesModule());
            builder.RegisterModule(new PhysicsModule());
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new AIModule());

            return builder.Build();
        }
    }
}
