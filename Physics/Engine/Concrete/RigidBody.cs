﻿using Kittan.Core.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Enums;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Engine.Concrete
{
    class RigidBody : IRigidBody
    {
        private float mass;
        private float friction;
        private bool isAffectedByGravity;
        private Vector2 position;
        private Vector2 velocity;
        private double rotation;    // radians
        private IAxisAlignedBoundingBox axisAlignedBoundingBox;
        private ICollisionMesh collisionMesh;
        private IRuntimeVariables runtimeVariables;

        public float Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        public float Friction
        {
            get { return friction; }
            set { friction = value; }
        }

        public bool IsAffectedByGravity
        {
            get { return isAffectedByGravity; }
            set { isAffectedByGravity = value; }
        }

        public Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                collisionMesh.Position = position;
                axisAlignedBoundingBox = collisionMesh.GetBoundingBoxForMesh();
            }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public Vector2 Momentum
        {
            get { return mass * velocity; }
        }

        public IAxisAlignedBoundingBox AxisAlignedBoundingBox
        {
            get { return axisAlignedBoundingBox; }
            set { axisAlignedBoundingBox = value; }
        }

        public ICollisionMesh CollisionMesh
        {
            get { return collisionMesh; }
            set { collisionMesh = value; }
        }

        public RigidBody(
            IAxisAlignedBoundingBox axisAlignedBoundingBox,
            ICollisionMesh collisionMesh,
            IRuntimeVariables runtimeVariables)
        {
            position = new Vector2(0, 0);
            velocity = new Vector2(0, 0);
            mass = 1;   // default mass to 1kg to avoid problems with mass of 0

            this.axisAlignedBoundingBox = axisAlignedBoundingBox;
            this.collisionMesh = collisionMesh;
            this.runtimeVariables = runtimeVariables;
        }

        public void AddForce(Vector2 force, ForceMode forceMode, GameTime gameTime)
        {
            var deltaTime = ((float)gameTime.ElapsedGameTime.Milliseconds / 1000) * runtimeVariables.TimeDilation;

            switch (forceMode)
            {
                case ForceMode.Force:
                    velocity += force * deltaTime;
                    break;
                case ForceMode.Impulse:
                    velocity += force;
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            var deltaTime = ((float)gameTime.ElapsedGameTime.Milliseconds / 1000) * runtimeVariables.TimeDilation;

            if (isAffectedByGravity)
            {
                velocity += new Vector2(0, mass * runtimeVariables.GravityAcceleration) * deltaTime;
            }

            position += velocity;
            collisionMesh.Position = position;
            axisAlignedBoundingBox = collisionMesh.GetBoundingBoxForMesh();
        }

        /// <summary>
        /// Returns the position of the rigid body after this update, without actually moving it.
        /// Useful for sweep tests.
        /// </summary>
        public Vector2 GetPositionAfterThisUpdate(GameTime gameTime)
        {
            var deltaTime = ((float)gameTime.ElapsedGameTime.Milliseconds / 1000) * runtimeVariables.TimeDilation;

            return isAffectedByGravity
                ? position + (velocity + (new Vector2(0, mass * runtimeVariables.GravityAcceleration) * deltaTime))
                : position + velocity;
        }
    }
}
