﻿using System;
using System.Collections.Generic;
using System.Linq;

using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.BoundingTree.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.Physics.Engine.Concrete
{
    /// <summary>
    /// This class provides the core physics functionality of the game engine
    /// </summary>
    public class PhysicsEngine : IPhysicsEngine
    {
        private Dictionary<Guid, IPhysicsObject> physicsObjects;
        private IBoundingTree boundingTree;
        private IRuntimeVariables runtimeVariables;
        private IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory;
        private SpriteBatch spriteBatch;

        public PhysicsEngine(
            IBoundingTree boundingTree,
            IRuntimeVariables runtimeVariables,
            IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory,
            SpriteBatch spriteBatch)
        {
            this.boundingTree = boundingTree;
            this.runtimeVariables = runtimeVariables;
            this.axisAlignedBoundingBoxFactory = axisAlignedBoundingBoxFactory;
            this.spriteBatch = spriteBatch;

            physicsObjects = new Dictionary<Guid, IPhysicsObject>();
        }

        public Guid AddPhysicsObject(IPhysicsObject newPhysicsObject)
        {
            var newId = Guid.NewGuid();

            // The chance of having a duplicate Guid is incredibly small, but better safe
            // than sorry!
            while (physicsObjects.ContainsKey(newId))
            {
                newId = Guid.NewGuid();
            }

            newPhysicsObject.PhysicsEngineId = newId;
            physicsObjects.Add(newId, newPhysicsObject);
            boundingTree.AddObject(newPhysicsObject, newId);

            return newId;
        }

        public void RemovePhysicsObject(Guid objectId)
        {
            physicsObjects.Remove(objectId);
            boundingTree.RemoveObject(objectId);
        }

        public void Clear(ClearLevel clearLevel)
        {
            var itemsToRemove = new List<Guid>();

            switch (clearLevel)
            {
                case ClearLevel.All:
                    ClearBoundingTree();
                    physicsObjects.Clear();
                    return;
                case ClearLevel.ObjectsOnly:
                    foreach (var currentObject in physicsObjects)
                    {
                        if (currentObject.Value.ObjectType == PhysicsObjectType.Object)
                        {
                            boundingTree.RemoveObject(currentObject.Key);
                            itemsToRemove.Add(currentObject.Key);
                        }
                    }
                    break;
                case ClearLevel.LevelOnly:
                    foreach (var currentObject in physicsObjects)
                    {
                        if (currentObject.Value.ObjectType == PhysicsObjectType.LevelCollider)
                        {
                            boundingTree.RemoveObject(currentObject.Key);
                            itemsToRemove.Add(currentObject.Key);
                        }
                    }
                    break;
            }

            foreach (var currentId in itemsToRemove)
            {
                physicsObjects.Remove(currentId);
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (var curObject in physicsObjects)
            {
                curObject.Value.Update(gameTime);
                boundingTree.UpdateObject(curObject.Value, curObject.Key);
            }

            // Construct list of collisions to be resolved
            var collisions = new List<ICollisionInfo>();
            var objects = physicsObjects.Where(o => o.Value.ObjectType != PhysicsObjectType.LevelCollider).ToList();
            foreach (var currentObject in objects)
            {
                var broadPhaseCollisions = boundingTree.GetCollisions(currentObject.Value, currentObject.Key);
                foreach (var otherObject in broadPhaseCollisions)
                {
                    // If this collision pair has not yet been checked/added to the collision list then do so
                    var collisionAlreadyChecked = collisions.Any(c =>
                        (c.FirstObjectId == currentObject.Key && c.SecondObjectId == otherObject) ||
                        (c.FirstObjectId == otherObject && c.SecondObjectId == currentObject.Key));
                    if (!collisionAlreadyChecked) {
                        if (currentObject.Value.CollidesWith(physicsObjects[otherObject], out ICollisionInfo collisionInfo))
                        {
                            collisions.Add(collisionInfo);
                        }
                    }
                }
            }

            foreach (var collision in collisions)
            {
                ResolveCollision(collision, gameTime);
            }
        }

        public void Render()
        {
            foreach (var curObject in physicsObjects)
            {
                curObject.Value.Render();
            }

            if (runtimeVariables.DebugModeEnabled)
            {
                boundingTree.DrawForDebug(spriteBatch, runtimeVariables.DebugTexture);
            }
        }

        // Returns true if the physics object is touching a level collider beneath it
        public bool IsObjectTouchingTheFloor(Guid physicsObjectId)
        {
            // Create a test volume that is marginally thinner than the original (to avoid detecting
            //  collisions to the sides) and extends marginally below, to add a bit of leeway.
            var testAABB = physicsObjects[physicsObjectId].RigidBody.AxisAlignedBoundingBox.Shrink(1.0f);
            testAABB.Max += new Vector2(0, 2.0f);

            var broadCollisions = boundingTree.TestCollisions(testAABB);
            foreach (var collision in broadCollisions)
            {
                if (physicsObjects[collision].PhysicsEngineId != physicsObjectId)
                    return true;
            }

            return false;
        }

        // Checks to see if a specified point is currently colliding with anything. Used for raycasting.
        public bool CheckIfPointCollides(Vector2 point, Guid? physicsObjectToIgnore = null)
        {
            // Construct a 1x1 AABB around the point so it can be tested using the bounding tree
            var testMin = point + new Vector2(-1, -1);
            var testMax = point + new Vector2(1, 1);

            var testAABB = axisAlignedBoundingBoxFactory.CreateAxisAlignedBoundingBox(testMin, testMax);

            return physicsObjectToIgnore.HasValue
                ? boundingTree.TestCollisions(testAABB).Where((c) => c != physicsObjectToIgnore.Value).Any()
                : boundingTree.TestCollisions(testAABB).Any();
        }

        public bool CheckIfPointCollidesWithPlayer(Vector2 point)
        {
            var player = physicsObjects.Where((po) => po.Value.ObjectType == PhysicsObjectType.Player);
            return player.Count() > 0
                ? CheckIfPointCollidesWithObject(point, player.First().Key)
                : false;
        }

        public bool CheckIfPointCollidesWithObject(Vector2 point, Guid physicsObjectId)
        {
            // Construct a 1x1 AABB around the point so it can be tested using the bounding tree
            var testMin = point + new Vector2(-1, -1);
            var testMax = point + new Vector2(1, 1);

            var testAABB = axisAlignedBoundingBoxFactory.CreateAxisAlignedBoundingBox(testMin, testMax);

            return boundingTree.TestCollisions(testAABB).Where((c) => c == physicsObjectId).Any();
        }

        private void ClearBoundingTree()
        {
            foreach (var curObject in physicsObjects)
            {
                boundingTree.RemoveObject(curObject.Key);
            }
        }

        private void ResolveCollision(ICollisionInfo collisionInfo, GameTime gameTime)
        {
            var firstObject = physicsObjects[collisionInfo.FirstObjectId];
            var secondObject = physicsObjects[collisionInfo.SecondObjectId];

            if (firstObject.ObjectType == PhysicsObjectType.Player && secondObject.ObjectType == PhysicsObjectType.EndZone ||
                firstObject.ObjectType == PhysicsObjectType.EndZone && secondObject.ObjectType == PhysicsObjectType.Player)
            {
                runtimeVariables.GameComplete = true;
            }
            else if (firstObject.ObjectType == PhysicsObjectType.Player && secondObject.ObjectType == PhysicsObjectType.NPC ||
                firstObject.ObjectType == PhysicsObjectType.NPC && secondObject.ObjectType == PhysicsObjectType.Player)
            {
                // If the player hits an enemy, send them back to the beginning
                if (firstObject.ObjectType == PhysicsObjectType.Player)
                {
                    firstObject.RigidBody.Velocity = new Vector2(0, 0);
                    firstObject.RigidBody.Position = runtimeVariables.PlayerRespawnLocation;
                }
                else
                {
                    secondObject.RigidBody.Velocity = new Vector2(0, 0);
                    secondObject.RigidBody.Position = runtimeVariables.PlayerRespawnLocation;
                }

                return;
            }

            var relativeVelocity = secondObject.RigidBody.Velocity - firstObject.RigidBody.Velocity;
            var velocityAlongCollisionNormal = Vector2.Dot(relativeVelocity, collisionInfo.ContactPoint.Normal);

            // If the objects are separating, return.
            if (velocityAlongCollisionNormal > 0)
               return;

            var restitution = 0.1f;    // Fixed bounciness for now. TODO: Add restitution property to RigidBodies

            var impulseScalar = -(1 + restitution) * velocityAlongCollisionNormal;
            var firstObjectInverseMass = firstObject.RigidBody.Mass > 0
                ? 1 / firstObject.RigidBody.Mass
                : 0;
            var secondObjectInverseMass = secondObject.RigidBody.Mass > 0
                ? 1 / secondObject.RigidBody.Mass
                : 0;

            impulseScalar = impulseScalar / (firstObjectInverseMass + secondObjectInverseMass);

            var impulse = impulseScalar * collisionInfo.ContactPoint.Normal;
            physicsObjects[collisionInfo.FirstObjectId].RigidBody.Velocity -= firstObjectInverseMass * impulse;
            physicsObjects[collisionInfo.SecondObjectId].RigidBody.Velocity += secondObjectInverseMass * impulse;

            // Correct penetration
            var threshold = 0.01f;
            var correctionPercentage = 0.8f;
            if (collisionInfo.ContactPoint.PenetrationDepth > threshold)
            {
                var correction = (collisionInfo.ContactPoint.PenetrationDepth / (firstObjectInverseMass + secondObjectInverseMass)) * correctionPercentage * collisionInfo.ContactPoint.Normal;
                firstObject.RigidBody.Position -= firstObjectInverseMass * correction;
                secondObject.RigidBody.Position += secondObjectInverseMass * correction;
            }

            // Apply friction. Only applies horizontally for now.
            var relativeXVelocity = secondObject.RigidBody.Velocity.X - firstObject.RigidBody.Velocity.X;

            if (firstObject.ObjectType != PhysicsObjectType.LevelCollider)
            {
                firstObject.RigidBody.AddForce(new Vector2(-firstObject.RigidBody.Velocity.X, 0) * secondObject.RigidBody.Friction, ForceMode.Force, gameTime);
                firstObject.RigidBody.AddForce(new Vector2(relativeXVelocity, 0) * secondObject.RigidBody.Friction, ForceMode.Force, gameTime);
            }

            if (secondObject.ObjectType != PhysicsObjectType.LevelCollider)
            {
                secondObject.RigidBody.AddForce(new Vector2(-secondObject.RigidBody.Velocity.X, 0) * firstObject.RigidBody.Friction, ForceMode.Force, gameTime);
                secondObject.RigidBody.AddForce(new Vector2(relativeXVelocity, 0) * firstObject.RigidBody.Friction, ForceMode.Force, gameTime);
            }

            boundingTree.UpdateObject(firstObject, firstObject.PhysicsEngineId);
            boundingTree.UpdateObject(secondObject, secondObject.PhysicsEngineId);
        }

        public bool CheckIfPointHasFloorBeneathIt(Vector2 point)
        {
            // Construct a 1x4 AABB around the point so it can be tested using the bounding tree
            var testMin = point + new Vector2(-1, -3);
            var testMax = point + new Vector2(1, 1);

            var testAABB = axisAlignedBoundingBoxFactory.CreateAxisAlignedBoundingBox(testMin, testMax);
            testAABB.Max += new Vector2(0, 2.0f);

            var broadCollisions = boundingTree.TestCollisions(testAABB);
            foreach (var collision in broadCollisions)
            {
                if (physicsObjects[collision].ObjectType == PhysicsObjectType.LevelCollider ||
                    physicsObjects[collision].ObjectType == PhysicsObjectType.Object)
                    return true;
            }

            return false;
        }
    }
}
