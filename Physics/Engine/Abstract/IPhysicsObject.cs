﻿using System;

using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Enums;

namespace Kittan.Physics.Engine.Abstract
{
	public interface IPhysicsObject : IGameObject
	{
        Guid PhysicsEngineId { get; set; }
        IRigidBody RigidBody { get; set; }
        PhysicsObjectType ObjectType { get; }

        bool CollidesWith(IPhysicsObject stationaryObject, out ICollisionInfo collisionInfo);
	}
}
