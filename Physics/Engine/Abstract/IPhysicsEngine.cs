﻿using System;

using Kittan.Physics.Enums;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Engine.Abstract
{
	public interface IPhysicsEngine
	{
		void Update(GameTime gameTime);
        void Render();
		Guid AddPhysicsObject(IPhysicsObject newPhysicsObject);
		void RemovePhysicsObject(Guid objectId);
        void Clear(ClearLevel clearLevel);
        bool IsObjectTouchingTheFloor(Guid physicsObjectId);
        bool CheckIfPointCollides(Vector2 point, Guid? physicsObjectToIgnore = null);
        bool CheckIfPointCollidesWithPlayer(Vector2 point);
        bool CheckIfPointCollidesWithObject(Vector2 point, Guid physicsObjectId);
        bool CheckIfPointHasFloorBeneathIt(Vector2 point);
    }
}
