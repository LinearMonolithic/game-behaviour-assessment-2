﻿using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Enums;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Engine.Abstract
{
    public interface IRigidBody
    {
        IAxisAlignedBoundingBox AxisAlignedBoundingBox { get; set; }
        ICollisionMesh CollisionMesh { get; set; }
        Vector2 Velocity { get; set; }
        Vector2 Momentum { get; }
        Vector2 Position { get; set; }
        float Mass { get; set; }
        float Friction { get; set; }
        bool IsAffectedByGravity { get; set; }

        void AddForce(Vector2 force, ForceMode forceMode, GameTime gameTime);
        void Update(GameTime gameTime);
        Vector2 GetPositionAfterThisUpdate(GameTime gameTime);
    }
}
