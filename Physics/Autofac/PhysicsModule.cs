﻿using Autofac;

using Kittan.Physics.Collision.Concrete;
using Kittan.Physics.Collision.BoundingTree.Concrete;
using Kittan.Physics.Collision.Raytracing.Concrete;
using Kittan.Physics.Engine.Concrete;
using Kittan.Physics.Factories.Concrete;

namespace Kittan.Physics.Autofac
{
	public class PhysicsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
            builder.RegisterType<CollisionMeshFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<NarrowPhaseCollisionResolverFactory>().AsImplementedInterfaces().SingleInstance();
			builder.RegisterType<PhysicsEngine>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<RigidBodyFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<AxisAlignedBoundingBoxFactory>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<Raycaster>().AsImplementedInterfaces().SingleInstance();

            builder.RegisterType<CollisionInfo>().AsImplementedInterfaces();
			builder.RegisterType<CollisionMesh>().AsImplementedInterfaces();
            builder.RegisterType<ContactPoint>().AsImplementedInterfaces();
            builder.RegisterType<RigidBody>().AsImplementedInterfaces();
            builder.RegisterType<BoundingTree>().AsImplementedInterfaces();
		}
	}
}
