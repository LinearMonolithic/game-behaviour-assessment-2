﻿namespace Kittan.Physics.Enums
{
    public enum PhysicsObjectType
    {
        Object,
        LevelCollider,
        Player,
        NPC,
        EndZone
    }
}
