﻿namespace Kittan.Physics.Enums
{
    public enum CollisionResolverType
    {
        AABB,
        SAT
    }
}
