﻿namespace Kittan.Physics.Enums
{
	/// <summary>
	/// Modes of adding force, used in the AddForce method of physics objects.
	/// Force - Add a continous force
	/// Impulse - Add an instantaneous force
	/// </summary>
	public enum ForceMode
	{
		Force,
		Impulse
	}
}
