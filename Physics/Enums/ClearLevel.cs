﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kittan.Physics.Enums
{
    public enum ClearLevel
    {
        ObjectsOnly,
        LevelOnly,
        All
    }
}
