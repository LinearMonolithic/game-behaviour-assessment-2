﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Raytracing.Abstract
{
    public interface IRaycaster
    {
        bool TestRay(Vector2 position, Vector2 target, Guid physicsObjectId);
        ICollisionInfo DetailedRay(Vector2 position, Vector2 target);
    }
}
