﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Raytracing.Abstract;
using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Raytracing.Concrete
{
    public class Ray : IRay
    {
        private Vector2 position;
        private Vector2 target;
        private Vector2 direction;
        private int distanceLeft;

        public Vector2 Position
        {
            get { return position; }
        }

        public Ray(Vector2 position, Vector2 target, int maxDistance)
        {
            this.position = position;
            this.target = target;
            distanceLeft = maxDistance;

            direction = target - position;
            direction.Normalize();
        }

        /// <summary>
        /// Update the ray position. If it's reached the target, or reached the target, return false
        /// </summary>
        public bool Update()
        {
            position += direction;
            --distanceLeft;

            return position == target || distanceLeft == 0
                ? false
                : true;
        }
    }
}
