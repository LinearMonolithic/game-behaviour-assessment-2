﻿using System;

using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.Raytracing.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Raytracing.Concrete
{
    public class Raycaster : IRaycaster
    {
        private IRuntimeVariables runtimeVariables;
        private IPhysicsEngine physicsEngine;

        public Raycaster(
            IRuntimeVariables runtimeVariables,
            IPhysicsEngine physicsEngine)
        {
            this.runtimeVariables = runtimeVariables;
            this.physicsEngine = physicsEngine;
        }

        public ICollisionInfo DetailedRay(Vector2 position, Vector2 target)
        {
            throw new NotImplementedException();
        }

        public bool TestRay(Vector2 position, Vector2 target, Guid physicsObjectId)
        {
            var ray = new Ray(position, target, runtimeVariables.MaximumRayDistance);

            while (ray.Update())
            {
                if (physicsEngine.CheckIfPointCollides(ray.Position, physicsObjectId))
                {
                    return (physicsEngine.CheckIfPointCollidesWithPlayer(ray.Position));
                }
            }

            return ray.Position == target;
        }
    }
}
