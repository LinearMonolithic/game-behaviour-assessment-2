﻿using System;

using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Concrete
{
    public class CollisionInfo : ICollisionInfo
    {
        //TODO: eventually allow multiple contact points?
        IContactPoint contactPoint;
        Guid firstObjectId;
        Guid secondObjectId;
        Vector2 minimumTranslationVector;

        public IContactPoint ContactPoint
        {
            get { return contactPoint; }
        }

        public Guid FirstObjectId
        {
            get { return firstObjectId; }
        }

        public Guid SecondObjectId
        {
            get { return secondObjectId; }
        }

        public Vector2 MTV
        {
            get { return minimumTranslationVector; }
        }

        public CollisionInfo(IContactPoint contactPoint, Guid firstObjectId, Guid secondObjectId, Vector2 minimumTranslationVector)
        {
            this.contactPoint = contactPoint;
            this.firstObjectId = firstObjectId;
            this.secondObjectId = secondObjectId;
            this.minimumTranslationVector = minimumTranslationVector;
        }
    }
}
