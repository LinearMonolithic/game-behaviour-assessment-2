﻿using System;

using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.Physics.Collision.Concrete
{
    public class AxisAlignedBoundingBox : IAxisAlignedBoundingBox
    {
        private Vector2 min;
        private Vector2 max;

        public Vector2 Min
        {
            get { return min; }
            set { min = value; }
        }

        public Vector2 Max
        {
            get { return max; }
            set { max = value; }
        }

        public float Width
        {
            get { return max.X - min.X; }
        }

        public float Height
        {
            get { return max.Y - min.Y; }
        }

        public float SurfaceArea
        {
            get { return Width * Height; }
        }

        public AxisAlignedBoundingBox(Vector2 min, Vector2 max)
        {
            this.min = min;
            this.max = max;
        }

        /// <summary>
        /// Returns true if this AABB and the other AABB intersect
        /// </summary>
        public bool CollidesWith(IAxisAlignedBoundingBox otherBox)
        {
            return !(
                min.X > otherBox.Max.X ||
                min.Y > otherBox.Max.Y ||
                max.X < otherBox.Min.X ||
                max.Y < otherBox.Min.Y);
        }

        /// <summary>
        /// Returns true if the other AABB is contained within this one.
        /// </summary>
        public bool Contains(IAxisAlignedBoundingBox otherBox)
        {
            return (
                otherBox.Max.X < max.X &&
                otherBox.Max.Y < max.Y &&
                otherBox.Min.X > min.X &&
                otherBox.Min.Y > min.Y);
        }


        /// <summary>
        /// Returns the AABB that contains both AABBs
        /// </summary>
        public IAxisAlignedBoundingBox Merge(IAxisAlignedBoundingBox otherBox, float padding = 0)
        {
            var newMin = new Vector2(
                min.X < otherBox.Min.X ? min.X - padding : otherBox.Min.X - padding,
                min.Y < otherBox.Min.Y ? min.Y - padding : otherBox.Min.Y - padding);
            var newMax = new Vector2(
                max.X > otherBox.Max.X ? max.X + padding : otherBox.Max.X + padding,
                max.Y > otherBox.Max.Y ? max.Y + padding : otherBox.Max.Y + padding);

            return new AxisAlignedBoundingBox(newMin, newMax);
        }

        public void DrawForDebug(SpriteBatch spriteBatch, Texture2D debugTexture)
        {
            spriteBatch.Draw(debugTexture, new Rectangle((int)Min.X, (int)Min.Y, 1, (int)Height), Color.Red);
            spriteBatch.Draw(debugTexture, new Rectangle((int)Max.X, (int)Min.Y, 1, (int)Height), Color.Red);
            spriteBatch.Draw(debugTexture, new Rectangle((int)Min.X, (int)Min.Y, (int)Width, 1), Color.Red);
            spriteBatch.Draw(debugTexture, new Rectangle((int)Min.X, (int)Max.Y, (int)Width, 1), Color.Red);
        }

        /// <summary>
        /// Return a new AABB, shrunk by the specified amount
        /// </summary>
        public IAxisAlignedBoundingBox Shrink(float shrinkAmount)
        {
            return new AxisAlignedBoundingBox(
                new Vector2(
                    Min.X + shrinkAmount,
                    Min.Y + shrinkAmount),
                new Vector2(
                    Max.X - shrinkAmount,
                    Max.Y - shrinkAmount));
        }
    }
}
