﻿using System;
using System.Collections.Generic;

using Kittan.General.Extensions;
using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;
using Kittan.Physics.Factories.Abstract;

namespace Kittan.Physics.Collision.Concrete
{
    /// <summary>
    /// This class represents the object geometry used for narrow phase
    /// collision. To reduce the number of necessary operations, this is saved 
    /// as a position vector and an array of points that make up the mesh. The
    /// latter are given relative to the position, such that moving the mesh
    /// involves only changing one vector rather than all points in the mesh.
    /// </summary>
	public class CollisionMesh : ICollisionMesh
    {
        private Vector2 meshPosition;       // The position of the mesh
        private List<Vector2> vertices;     // List of points that describe the edges that make up the mesh, given in clockwise order
        private IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory;

        public Vector2 Position
        {
            get { return meshPosition; }
            set { meshPosition = value; }
        }

        public List<Vector2> Vertices
        {
            get { return GetWorldSpaceVertices(); }
        }

        public CollisionMesh(
            Vector2 meshPosition, 
            List<Vector2> vertices,
            IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory)
        {
            this.meshPosition = meshPosition;
            this.vertices = vertices;
            this.axisAlignedBoundingBoxFactory = axisAlignedBoundingBoxFactory;
        }

        /// <summary>
        /// Rotate the collision mesh
        /// </summary>
        /// <param name="pivotPoint">The point around which the rotation should be performed</param>
        /// <param name="angle">The angle of the rotation in radians</param>
        public void Rotate(Vector2 pivotPoint, double angle)
        {
            float sinAngle = (float)Math.Sin(angle);
            float cosAngle = (float)Math.Cos(angle);

            vertices.ForEach((vertex) =>
            {
                //translate pivot point to origin
                vertex.X -= pivotPoint.X;
                vertex.Y -= pivotPoint.Y;

                //rotate point
                var rotatedX = vertex.X * cosAngle - vertex.Y * sinAngle;
                var rotatedY = vertex.X * sinAngle + vertex.Y * cosAngle;

                //translate point back
                vertex.X = rotatedX + pivotPoint.X;
                vertex.Y = rotatedY + pivotPoint.Y;
            });
        }

        /// <summary>
        /// Translate the collision mesh
        /// </summary>
        /// <param name="translation">The vector describing the translation</param>
        public void Translate(Vector2 translation)
        {
            meshPosition += translation;
        }

        /// <summary>
        /// Generate an axis-aligned bounding box for this collision mesh
        /// </summary>
        /// <returns>A new AxisAlignedBoundingBox instance</returns>
        public IAxisAlignedBoundingBox GetBoundingBoxForMesh()
        {
            Vector2 min = new Vector2(vertices[0].X, vertices[0].Y);
            Vector2 max = new Vector2(vertices[0].X, vertices[0].Y);

            foreach (var vertex in vertices)
            {
                if (vertex.X < min.X)
                    min.X = vertex.X;
                if (vertex.Y < min.Y)
                    min.Y = vertex.Y;
                if (vertex.X > max.X)
                    max.X = vertex.X;
                if (vertex.Y > max.Y)
                    max.Y = vertex.Y;
            }

            return axisAlignedBoundingBoxFactory.CreateAxisAlignedBoundingBox(meshPosition + min, meshPosition + max);
        }

        /// <summary>
        /// Convert the relative positions of the vertices into their actual
        /// world-space positions.
        /// </summary>
        private List<Vector2> GetWorldSpaceVertices()
        {
            var worldSpaceVertices = new List<Vector2>();

            foreach(var vertex in vertices)
            {
                worldSpaceVertices.Add(meshPosition + vertex);
            }

            return worldSpaceVertices;
        }
    }
}
