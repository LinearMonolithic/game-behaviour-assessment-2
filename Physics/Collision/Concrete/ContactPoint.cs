﻿using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Concrete
{
    /// <summary>
    /// This class represents a point of contact between two rigidbodies
    /// </summary>
    public class ContactPoint : IContactPoint
    {
        Vector2 position;       // Position of the collision in world space
        Vector2 normal;         // Normal of the collision
        float penetrationDepth; // Penetration depth of the collision

        public Vector2 Position
        {
            get { return position; }
        }

        public Vector2 Normal
        {
            get { return normal; }
        }

        public float PenetrationDepth
        {
            get { return penetrationDepth; }
        }

        public ContactPoint(Vector2 position, Vector2 normal, float penetrationDepth)
        {
            this.position = position;
            this.normal = normal;
            this.penetrationDepth = penetrationDepth;
        }
    }
}
