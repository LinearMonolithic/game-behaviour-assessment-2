﻿using System.Collections.Generic;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework;
using Kittan.Physics.Collision.Concrete;
using Kittan.Physics.Collision.SeparatingAxisTheorem.Abstract;

namespace Kittan.Physics.Collision.SeparatingAxisTheorem.Concrete
{
    public class SeparatingAxisTheoremCollisionResolver : INarrowPhaseCollisionResolver
    {
        /// <summary>
        /// Tests if two physics objects collides or not and works out the
        /// collision point, penetration depth etc.
        /// </summary>
        /// <returns></returns>
        public bool TestCollision(IPhysicsObject firstObject, IPhysicsObject secondObject, out ICollisionInfo collisionInfo)
        {
            var axesToTest = new List<IAxis>();
            var firstObjectVertices = firstObject.RigidBody.CollisionMesh.Vertices;
            var secondObjectVertices = secondObject.RigidBody.CollisionMesh.Vertices;

            IAxis smallestAxis = null;
            float? smallestOverlap = null;
            Vector2 collisionPoint = new Vector2();
            collisionInfo = null;

            // Get the axes to test by calculating the normals for each edge of each mesh
            axesToTest.AddRange(GetAxesForMesh(firstObjectVertices));
            axesToTest.AddRange(GetAxesForMesh(secondObjectVertices));

            foreach (var axis in axesToTest)
            {
                // Project each mesh onto the axis
                var movingObjectProjection = axis.ProjectMeshOntoAxis(firstObjectVertices);
                var stationaryObjectProjection = axis.ProjectMeshOntoAxis(secondObjectVertices);

                // If at any point the projections don't overlap, the two meshes cannot be intersecting
                if (!movingObjectProjection.Overlaps(stationaryObjectProjection))
                {
                    return false;
                }

                // Otherwise, calculate the overlap and check to see if this is the smallest axis
                var tempOverlap = movingObjectProjection.GetOverlap(stationaryObjectProjection, out int collisionIndex);

                if (tempOverlap < smallestOverlap || !smallestOverlap.HasValue)
                {
                    smallestOverlap = tempOverlap;
                    smallestAxis = axis;
                    collisionPoint = firstObjectVertices[collisionIndex];
                }
            }

            // Project the distance between the two objects onto the smallest axis and adjust the normal accordingly.
            // Since the first object should be 'bouncing off' the second insofar as our impulse calculation is concerned,
            // make sure the collision normal is pointing 'towards' the first object.
            var distance = firstObject.RigidBody.Position - secondObject.RigidBody.Position;
            var collisionNormal = new Vector2(smallestAxis.VertexNormal.X, smallestAxis.VertexNormal.Y);
            if(Vector2.Dot(distance, smallestAxis.VertexNormal) > 0)
            {
                collisionNormal.X = -collisionNormal.X;
                collisionNormal.Y = -collisionNormal.Y;
            }

            var penetrationDepth = smallestOverlap.Value;

            var contactPoint = new ContactPoint(
                collisionPoint + collisionNormal * penetrationDepth,
                collisionNormal,
                penetrationDepth);

            collisionInfo = new CollisionInfo(
                contactPoint, 
                firstObject.PhysicsEngineId, 
                secondObject.PhysicsEngineId,
                collisionNormal * penetrationDepth);

            return true;
        }

        private List<IAxis> GetAxesForMesh(List<Vector2> vertices)
        {
            var axes = new List<IAxis>();

            for (int i = 0; i < vertices.Count; ++i)
            {
                var currentAxis = (i == vertices.Count - 1)
                    ? new Axis(vertices[i], vertices[0])
                    : new Axis(vertices[i], vertices[i + 1]);

                axes.Add(currentAxis);
            }

            return axes;
        }
    }
}
