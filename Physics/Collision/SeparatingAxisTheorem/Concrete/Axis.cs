﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.General.Extensions;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.SeparatingAxisTheorem.Abstract;

using Microsoft.Xna.Framework;
using Kittan.Physics.Collision.Concrete;

namespace Kittan.Physics.Collision.SeparatingAxisTheorem.Concrete
{
    public class Axis : IAxis
    {
        private Vector2 originalVertex;
        private Vector2 vertexNormal;

        public Axis(Vector2 point1, Vector2 point2)
        {
            originalVertex = point2 - point1;
            vertexNormal = originalVertex.GetNormal();
            vertexNormal.Normalize();
        }

        public Vector2 VertexNormal
        {
            get { return vertexNormal; }
        }

        public IProjection ProjectMeshOntoAxis(List<Vector2> vertices)
        {
            // Initialise variables
            float min = Vector2.Dot(vertexNormal, vertices[0]);
            float max = min;
            int minIndex = 0;
            int maxIndex = 0;

            for (int i = 0; i < vertices.Count; ++i)
            {
                // Project vertex onto the axis
                float pointOnAxis = Vector2.Dot(vertexNormal, vertices[i]);

                if (pointOnAxis < min)
                {
                    min = pointOnAxis;
                    minIndex = i;
                }
                else if (pointOnAxis > max)
                {
                    max = pointOnAxis;
                    maxIndex = i;
                }
            }

            // return the final projection
            return new Projection(min, max, minIndex, maxIndex);
        }
    }
}
