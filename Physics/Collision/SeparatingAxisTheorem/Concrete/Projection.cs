﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Abstract;

namespace Kittan.Physics.Collision.Concrete
{
    /// <summary>
    /// This class represents a projection of a mesh onto an axis and stores
    /// the minimum and maximum values the shape reaches on the axis.
    /// </summary>
    public class Projection : IProjection
    {
        private float min;
        private float max;
        private int minIndex;
        private int maxIndex;

        public float Min
        {
            get { return min; }
        }

        public float Max
        {
            get { return max; }
        }

        public int MinIndex
        {
            get { return minIndex; }
        }

        public int MaxIndex
        {
            get { return maxIndex; }
        }

        public Projection(float min, float max, int minIndex, int maxIndex)
        {
            this.min = min;
            this.max = max;
            this.minIndex = minIndex;
            this.maxIndex = maxIndex;
        }

        public bool Overlaps(IProjection otherProjection)
        {
            return min < otherProjection.Max || max > otherProjection.Min;
        }

        /// <summary>
        /// Returns the amount one projection overlaps another projection.
        /// Also returns the index of the point which will be treated as the
        /// collision point as an out variable.
        /// </summary>
        public float GetOverlap(IProjection otherProjection, out int collisionIndex)
        {
            collisionIndex = 0;

            if (max < otherProjection.Max)
            {
                collisionIndex = minIndex;
                // Check to see if this projection is entirely enclosed
                return min > otherProjection.Min
                    ? max - min
                    : max - otherProjection.Min;
            }
            else
            {
                collisionIndex = maxIndex;
                // Check to see if the other projection is entirely enclosed
                return otherProjection.Min > min
                    ? otherProjection.Max - otherProjection.Min
                    : otherProjection.Max - min;
            }
        }
    }
}
