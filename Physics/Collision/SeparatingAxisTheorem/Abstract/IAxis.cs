﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.SeparatingAxisTheorem.Abstract
{
    public interface IAxis
    {
        Vector2 VertexNormal { get; }

        IProjection ProjectMeshOntoAxis(List<Vector2> vertices);
    }
}
