﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kittan.Physics.Collision.Abstract
{
    public interface IProjection
    {
        float Min { get; }
        float Max { get; }
        int MinIndex { get; }
        int MaxIndex { get; }

        bool Overlaps(IProjection otherProjection);
        float GetOverlap(IProjection otherProjection, out int collisionIndex);
    }
}
