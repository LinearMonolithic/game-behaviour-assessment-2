﻿using System;
using System.Collections.Generic;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework.Graphics;

namespace Kittan.Physics.Collision.BoundingTree.Abstract
{
    public interface IBoundingTree
    {
        void AddObject(IPhysicsObject newPhysicsObject, Guid newPhysicsObjectId);
        void RemoveObject(Guid physicsObjectId);
        void UpdateObject(IPhysicsObject physicsObject, Guid physicsObjectId);
        List<Guid> GetCollisions(IPhysicsObject objectToTest, Guid objectPhysicsId);
        List<Guid> TestCollisions(IAxisAlignedBoundingBox testVolume);
        void DrawForDebug(SpriteBatch spriteBatch, Texture2D debugTexture);
    }
}
