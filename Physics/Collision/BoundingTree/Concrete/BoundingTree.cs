﻿using System;
using System.Linq;
using System.Collections.Generic;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.BoundingTree.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework.Graphics;
using Kittan.Core.Abstract;

namespace Kittan.Physics.Collision.BoundingTree.Concrete
{
    /// <summary>
    /// This class represents a dynamic AABB tree, and was based on code by James Randall
    /// https://github.com/JamesRandall/SimpleVoxelEngine/blob/master/voxelEngine/src/AABBTree.cpp
    /// </summary>
    public class BoundingTree : IBoundingTree
    {
        private IRuntimeVariables runtimeVariables;

        private Guid? rootNodeId;
        private Dictionary<Guid, Node> tree;

        public BoundingTree(IRuntimeVariables runtimeVariables)
        {
            this.runtimeVariables = runtimeVariables;
        }

        /// <summary>
        /// Insert a new rigidbody into the tree
        /// </summary>
        public void AddObject(IPhysicsObject newPhysicsObject, Guid newPhysicsObjectId)
        {
            var newNode = new Node(GetNewNodeId(), newPhysicsObject.RigidBody.AxisAlignedBoundingBox, newPhysicsObjectId);

            InsertLeaf(newNode);
        }

        public void RemoveObject(Guid physicsObjectId)
        {
            var leafNode = tree.Single(n => n.Value.PhysicsObjectId == physicsObjectId);

            RemoveLeaf(leafNode.Value);
        }

        public void UpdateObject(IPhysicsObject physicsObject, Guid physicsObjectId)
        {
            var leafId = tree.Single(n => n.Value.PhysicsObjectId == physicsObjectId).Key;

            UpdateLeaf(leafId, physicsObject.RigidBody.AxisAlignedBoundingBox);
        }

        public List<Guid> GetCollisions(IPhysicsObject objectToTest, Guid objectPhysicsId)
        {
            var collisions = new List<Guid>();
            var stack = new Stack<Guid?>();
            var testAABB = objectToTest.RigidBody.AxisAlignedBoundingBox;

            stack.Push(rootNodeId);
            while (stack.Any())
            {
                var nodeId = stack.Pop();

                if (!nodeId.HasValue)
                {
                    continue;
                }

                var currentNode = tree[nodeId.Value];

                if (currentNode.AxisAlignedBoundingBox.CollidesWith(testAABB))
                {
                    if (currentNode.IsLeafNode && currentNode.PhysicsObjectId != objectPhysicsId)
                    {
                        collisions.Add(currentNode.PhysicsObjectId.Value);
                    }
                    else
                    {
                        stack.Push(currentNode.LeftChildId);
                        stack.Push(currentNode.RightChildId);
                    }
                }
            }

            return collisions;
        }

        public List<Guid> TestCollisions(IAxisAlignedBoundingBox testVolume)
        {
            var collisions = new List<Guid>();
            var stack = new Stack<Guid?>();

            stack.Push(rootNodeId);
            while (stack.Any())
            {
                var nodeId = stack.Pop();

                if (!nodeId.HasValue)
                {
                    continue;
                }

                var currentNode = tree[nodeId.Value];

                if (currentNode.AxisAlignedBoundingBox.CollidesWith(testVolume))
                {
                    if (currentNode.IsLeafNode)
                    {
                        collisions.Add(currentNode.PhysicsObjectId.Value);
                    }
                    else
                    {
                        stack.Push(currentNode.LeftChildId);
                        stack.Push(currentNode.RightChildId);
                    }
                }
            }

            return collisions;
        }

        public void DrawForDebug(SpriteBatch spriteBatch, Texture2D debugTexture)
        {
            foreach(var currentNode in tree)
            {
                currentNode.Value.AxisAlignedBoundingBox.DrawForDebug(spriteBatch, debugTexture);
            }
        }

        void InsertLeaf(Node leafNode)
        {
            // If the tree is currently empty, make this the root node
            if (tree == null || tree.Count == 0)
            {
                tree = new Dictionary<Guid, Node>();
                tree.Add(leafNode.Id, leafNode);
                rootNodeId = leafNode.Id;
                return;
            }

            // Use surface area and depth heuristics to choose the best place to insert the new leaf
            var currentNodeId = rootNodeId;
            var currentNode = tree[currentNodeId.Value];
            while (!currentNode.IsLeafNode)
            {
                // Since a node must always contain two children if it's a branch, we don't need null checks here
                var leftNode = tree[currentNode.LeftChildId.Value];
                var rightNode = tree[currentNode.RightChildId.Value];

                var combinedAABB = currentNode.AxisAlignedBoundingBox.Merge(leafNode.AxisAlignedBoundingBox);

                var newParentNodeCost = 2.0f * combinedAABB.SurfaceArea;
                var minimumPushDownCost = 2.0f * (combinedAABB.SurfaceArea - currentNode.AxisAlignedBoundingBox.SurfaceArea);

                // Use these calculated costs to decide whether we should create a new parent here or descend
                float costLeft;
                float costRight;
                if (leftNode.IsLeafNode)
                {
                    costLeft = leafNode.AxisAlignedBoundingBox.Merge(leftNode.AxisAlignedBoundingBox).SurfaceArea + minimumPushDownCost;
                }
                else
                {
                    var newLeftAABB = leafNode.AxisAlignedBoundingBox.Merge(leftNode.AxisAlignedBoundingBox);
                    costLeft = (newLeftAABB.SurfaceArea - leftNode.AxisAlignedBoundingBox.SurfaceArea) + minimumPushDownCost;
                }

                if (rightNode.IsLeafNode)
                {
                    costRight = leafNode.AxisAlignedBoundingBox.Merge(rightNode.AxisAlignedBoundingBox).SurfaceArea + minimumPushDownCost;
                }
                else
                {
                    var newRightAABB = leafNode.AxisAlignedBoundingBox.Merge(rightNode.AxisAlignedBoundingBox);
                    costRight = (newRightAABB.SurfaceArea - rightNode.AxisAlignedBoundingBox.SurfaceArea) + minimumPushDownCost;
                }

                // If the cost of creating a new parent node is less than descending in either direction then do so and attach the new leaf to it
                if (newParentNodeCost < costLeft && newParentNodeCost < costRight)
                {
                    break;
                }

                // Otherwise, descend in the cheapest direction
                currentNode = costLeft < costRight
                    ? leftNode
                    : rightNode;
            }

            // Create a new parent node and attach the leaf and the node found above
            var leafSibling = currentNode;
            var oldParentId = leafSibling.ParentId;
            var newParent = new Node(
                GetNewNodeId(), 
                leafNode.AxisAlignedBoundingBox.Merge(
                    leafSibling.AxisAlignedBoundingBox, 
                    runtimeVariables.BoundingBoxPadding), 
                leafNode.Id, 
                leafSibling.Id);
            newParent.ParentId = oldParentId;
            leafNode.ParentId = newParent.Id;
            leafSibling.ParentId = newParent.Id;

            if (oldParentId == null)
            {
                // the old parent was the root, so update the root variable to reflect rhe new root node
                rootNodeId = newParent.Id;
            }
            else
            {
                // patch the parent's left/right child Id
                var oldParent = tree[oldParentId.Value];
                if (oldParent.LeftChildId == leafSibling.Id)
                {
                    oldParent.LeftChildId = newParent.Id;
                }
                else
                {
                    oldParent.RightChildId = newParent.Id;
                }
            }

            // Add the node to the list
            tree.Add(leafNode.Id, leafNode);
            // Add the new parent to the tree
            tree.Add(newParent.Id, newParent);

            // Finally, walk back up the tree, fixing heights and areas
            FixUpwardsTree(newParent.Id);
        }

        void RemoveLeaf(Node leafNode)
        {
            // If the leaf is the root, clear the list and return
            if (leafNode.Id == rootNodeId)
            {
                rootNodeId = null;
                tree.Clear();
                return;
            }

            var parentNode = tree[leafNode.ParentId.Value];
            var grandparentNodeId = parentNode.ParentId;
            var siblingNodeId = parentNode.LeftChildId == leafNode.Id
                ? parentNode.RightChildId
                : parentNode.LeftChildId;
            var siblingNode = tree[siblingNodeId.Value];

            if (grandparentNodeId != null)
            {
                // If we have a grandparent, destroy the parent and connect the sibling to the grandparent
                var grandparentNode = tree[grandparentNodeId.Value];
                if (grandparentNode.LeftChildId == parentNode.Id)
                {
                    grandparentNode.LeftChildId = siblingNodeId;
                }
                else
                {
                    grandparentNode.RightChildId = siblingNodeId;
                }

                siblingNode.ParentId = grandparentNodeId;
                tree.Remove(parentNode.Id);

                FixUpwardsTree(grandparentNodeId);
            }
            else
            {
                // If we have no grandparent then the parent is the root. Make the sibling the root and remove its parent
                rootNodeId = siblingNodeId;
                siblingNode.ParentId = null;
                tree.Remove(parentNode.Id);
            }

            tree.Remove(leafNode.Id);
        }

        void UpdateLeaf(Guid leafId, IAxisAlignedBoundingBox axisAlignedBoundingBox)
        {
            var leafNode = tree[leafId];
            var physicsObjectId = leafNode.PhysicsObjectId;

            // If the node contains the new AABB then leave things alone.
            if (leafNode.AxisAlignedBoundingBox.Contains(axisAlignedBoundingBox))
            {
                return;
            }

            RemoveLeaf(leafNode);
            var newNode = new Node(GetNewNodeId(), axisAlignedBoundingBox, physicsObjectId.Value);
            InsertLeaf(newNode);
        }

        void FixUpwardsTree(Guid? currentNodeId)
        {
            while (currentNodeId != null)
            {
                var currentNode = tree[currentNodeId.Value];

                // Fix AABB
                var leftNode = tree[currentNode.LeftChildId.Value];
                var rightNode = tree[currentNode.RightChildId.Value];
                currentNode.AxisAlignedBoundingBox = leftNode.AxisAlignedBoundingBox.Merge(rightNode.AxisAlignedBoundingBox, runtimeVariables.BoundingBoxPadding);

                currentNodeId = currentNode.ParentId;
            }
        }

        Guid GetNewNodeId()
        {
            var newNodeId = Guid.NewGuid();

            // Just in case we somehow generate a duplicate Guid
            while (tree != null && tree.ContainsKey(newNodeId))
            {
                newNodeId = Guid.NewGuid();
            }

            return newNodeId;
        }
    }
}
