﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.BoundingTree.Abstract;

namespace Kittan.Physics.Collision.BoundingTree.Concrete
{
    /// <summary>
    /// This class represents a node in the dynamic AABB bounding tree.
    /// </summary>
    public class Node
    {
        Guid id;
        Guid? parentId;
        Guid? leftChildId;
        Guid? rightChildId;
        Guid? physicsObjectId;
        IAxisAlignedBoundingBox axisAlignedBoundingBox;
        int height;

        public Guid Id
        {
            get { return id; }
        }

        public Guid? ParentId
        {
            get { return parentId; }
            set { parentId = value; }
        }

        public Guid? LeftChildId
        {
            get { return leftChildId; }
            set { leftChildId = value; }
        }

        public Guid? RightChildId
        {
            get { return rightChildId; }
            set { rightChildId = value; }
        }

        public Guid? PhysicsObjectId
        {
            get { return physicsObjectId; }
        }

        public IAxisAlignedBoundingBox AxisAlignedBoundingBox
        {
            get { return axisAlignedBoundingBox; }
            set { axisAlignedBoundingBox = value; }
        }

        /// <summary>
        /// Constructor used when adding a leaf node
        /// </summary>
        public Node(Guid id, IAxisAlignedBoundingBox axisAlignedBoundingBox, Guid physicsObjectId)
        {
            this.id = id;
            this.axisAlignedBoundingBox = axisAlignedBoundingBox;
            this.physicsObjectId = physicsObjectId;
        }

        /// <summary>
        /// Constructor used when adding a branch node
        /// </summary>
        public Node(Guid id, IAxisAlignedBoundingBox axisAlignedBoundingBox, Guid leftChildId, Guid rightChildId)
        {
            this.id = id;
            this.leftChildId = leftChildId;
            this.rightChildId = rightChildId;
        }

        public bool IsLeafNode
        {
            get { return physicsObjectId.HasValue; }
        }
    }
}
