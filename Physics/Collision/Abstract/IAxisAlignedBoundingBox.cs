﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.Physics.Collision.Abstract
{
	public interface IAxisAlignedBoundingBox
	{
        Vector2 Min { get; set; }
        Vector2 Max { get; set; }
        float Width { get; }
        float Height { get; }
        float SurfaceArea { get; }

		bool CollidesWith(IAxisAlignedBoundingBox otherBox);
        bool Contains(IAxisAlignedBoundingBox otherBox);
        IAxisAlignedBoundingBox Merge(IAxisAlignedBoundingBox otherBox, float padding = 0);
        IAxisAlignedBoundingBox Shrink(float shrinkAmount);

        void DrawForDebug(SpriteBatch spriteBatch, Texture2D debugTexture);
	}
}
