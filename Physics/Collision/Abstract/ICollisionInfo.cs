﻿using System;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Abstract
{
    public interface ICollisionInfo
    {
        IContactPoint ContactPoint { get; }
        Guid FirstObjectId { get; }
        Guid SecondObjectId { get; }
        Vector2 MTV { get; }
    }
}
