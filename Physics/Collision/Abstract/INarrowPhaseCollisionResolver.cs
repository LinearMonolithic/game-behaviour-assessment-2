﻿using Kittan.Physics.Engine.Abstract;

namespace Kittan.Physics.Collision.Abstract
{
    public interface INarrowPhaseCollisionResolver
    {
        bool TestCollision(IPhysicsObject movingObject, IPhysicsObject stationaryObject, out ICollisionInfo collisionInfo);
    }
}
