﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Abstract
{
    public interface IContactPoint
    {
        Vector2 Position { get; }
        Vector2 Normal { get; }
        float PenetrationDepth { get; }
    }
}
