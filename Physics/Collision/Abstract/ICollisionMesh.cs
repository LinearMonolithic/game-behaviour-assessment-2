﻿using System.Collections.Generic;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Collision.Abstract
{
	public interface ICollisionMesh
	{
        Vector2 Position { get; set; }
        List<Vector2> Vertices { get; }

		void Rotate(Vector2 pivotPosition, double angle);
		void Translate(Vector2 translation);
		IAxisAlignedBoundingBox GetBoundingBoxForMesh();
	}
}
