﻿using System.Collections.Generic;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.Concrete;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Concrete
{
    public class CollisionMeshFactory : ICollisionMeshFactory
    {
        private IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory;

        public CollisionMeshFactory(IAxisAlignedBoundingBoxFactory axisAlignedBoundingBoxFactory)
        {
            this.axisAlignedBoundingBoxFactory = axisAlignedBoundingBoxFactory;
        }

        public ICollisionMesh CreateCollisionMesh(Vector2 position, List<Vector2> vertices)
        {
            return new CollisionMesh(position, vertices, axisAlignedBoundingBoxFactory);
        }
    }
}
