﻿using Kittan.Core.Abstract;
using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;
using Kittan.Physics.Factories.Abstract;
using Kittan.Physics.Engine.Concrete;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Concrete
{
    public class RigidBodyFactory : IRigidBodyFactory
    {
        private IRuntimeVariables runtimeVariables;

        public RigidBodyFactory(IRuntimeVariables runtimeVariables)
        {
            this.runtimeVariables = runtimeVariables;
        }

        public IRigidBody CreateRigidBody(
            Vector2 position, 
            bool isAffectedByGravity,  
            float mass,
            ICollisionMesh collisionMesh)
        {
            var rigidBody = new RigidBody(collisionMesh.GetBoundingBoxForMesh(), collisionMesh, runtimeVariables);
            rigidBody.Position = position;
            rigidBody.IsAffectedByGravity = isAffectedByGravity;
            rigidBody.Mass = mass;
            rigidBody.Friction = 50.0f;

            return rigidBody;
        }
    }
}
