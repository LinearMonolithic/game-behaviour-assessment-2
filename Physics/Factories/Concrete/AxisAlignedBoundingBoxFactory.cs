﻿using System;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Collision.Concrete;
using Kittan.Physics.Factories.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Concrete
{
    public class AxisAlignedBoundingBoxFactory : IAxisAlignedBoundingBoxFactory
    {
        public IAxisAlignedBoundingBox CreateAxisAlignedBoundingBox(Vector2 min, Vector2 max)
        {
            return new AxisAlignedBoundingBox(min, max);
        }
    }
}
