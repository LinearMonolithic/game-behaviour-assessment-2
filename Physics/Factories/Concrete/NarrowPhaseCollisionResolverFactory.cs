﻿using System;
using System.Collections.Generic;
using System.Text;

using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Enums;
using Kittan.Physics.Factories.Abstract;
using Kittan.Physics.Collision.SeparatingAxisTheorem.Concrete;

namespace Kittan.Physics.Factories.Concrete
{
    public class NarrowPhaseCollisionResolverFactory : INarrowPhaseCollisionResolverFactory
    {
        public INarrowPhaseCollisionResolver CreateNarrowPhaseCollisionResolver(CollisionResolverType collisionResolverType)
        {
            switch (collisionResolverType)
            {
                case CollisionResolverType.SAT:
                    return new SeparatingAxisTheoremCollisionResolver();
                default:
                    throw new NotImplementedException("This collision resolver type does not have an associated narrow phase collision resolver.");
            }
        }
    }
}
