﻿using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Engine.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Abstract
{
    public interface IRigidBodyFactory
    {
        IRigidBody CreateRigidBody(Vector2 position, bool isAffectedByGravity, float mass, ICollisionMesh collisionMesh);
    }
}
