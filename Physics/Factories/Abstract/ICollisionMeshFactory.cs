﻿using System.Collections.Generic;

using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Abstract
{
    public interface ICollisionMeshFactory
    {
        ICollisionMesh CreateCollisionMesh(Vector2 position, List<Vector2> vertices);
    }
}
