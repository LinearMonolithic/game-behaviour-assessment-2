﻿using Kittan.Physics.Collision.Abstract;

using Microsoft.Xna.Framework;

namespace Kittan.Physics.Factories.Abstract
{
    public interface IAxisAlignedBoundingBoxFactory
    {
        IAxisAlignedBoundingBox CreateAxisAlignedBoundingBox(Vector2 min, Vector2 max);
    }
}
