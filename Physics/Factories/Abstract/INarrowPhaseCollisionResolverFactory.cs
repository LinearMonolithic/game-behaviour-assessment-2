﻿using Kittan.Physics.Collision.Abstract;
using Kittan.Physics.Enums;

namespace Kittan.Physics.Factories.Abstract
{
    public interface INarrowPhaseCollisionResolverFactory
    {
        INarrowPhaseCollisionResolver CreateNarrowPhaseCollisionResolver(CollisionResolverType collisionResolverType);
    }
}
