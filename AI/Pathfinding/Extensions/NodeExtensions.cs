﻿using System;

using Kittan.AI.Pathfinding.Abstract;

namespace Kittan.AI.Pathfinding.Extensions
{
	public static class NodeExtensions
	{
		public static float GetDistanceTo(this INode parentNode, INode childNode)
		{
			return (float)Math.Sqrt(Math.Pow(parentNode.X - childNode.X, 2) + Math.Pow(parentNode.Y - childNode.Y, 2));
		}
	}
}
