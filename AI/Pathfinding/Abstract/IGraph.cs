﻿using System.Collections.Generic;

using Kittan.AI.Enums;
using Kittan.AI.Pathfinding.Concrete;

using Microsoft.Xna.Framework;

namespace Kittan.AI.Pathfinding.Abstract
{
    public interface IGraph
    {
        List<INode> Nodes { get; }
        List<IEdge> Edges { get; }

        void AddNode(int x, int y, NodeType nodeType);
        void RemoveNode(INode node);
        void AddEdge(INode parentNode, INode childNode, bool biDirectional, EdgeType edgeType);
        void RemoveEdge(IEdge edge);
        List<INode> GetChildren(INode parentNode);
        List<IEdge> GetEdges(INode parentNode);
        float GetHeuristic(INode currentNode, INode goalNode);
        List<IEdge> FindPath(INode startNode, INode goalNode);
        Node GetClosestNodeToPoint(Vector2 point);
        Node CreateTemporaryNode(Vector2 point);
    }
}
