﻿using Kittan.AI.Enums;

namespace Kittan.AI.Pathfinding.Abstract
{
    public interface IEdge
    {
        INode ParentNode { get; set; }
        INode ChildNode { get; set; }
        INode GoalNode { get; set; }   // Only used for pathfinding
        EdgeType EdgeType { get; set; }
        bool Bidirectional { get; set; }
        float Distance { get; }
        float Cost { get; }
    }
}
