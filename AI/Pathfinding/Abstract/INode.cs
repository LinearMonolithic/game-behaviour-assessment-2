﻿using Kittan.AI.Enums;

namespace Kittan.AI.Pathfinding.Abstract
{
	public interface INode
	{
		float X { get; set; }
		float Y { get; set; }
		float Distance { get; set; }
        NodeType NodeType { get; set; }
	}
}
