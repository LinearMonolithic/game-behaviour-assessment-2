﻿using Kittan.AI.Enums;
using Kittan.AI.Pathfinding.Abstract;
using Kittan.AI.Pathfinding.Extensions;

namespace Kittan.AI.Pathfinding.Concrete
{
	public class Edge : IEdge
	{
		private INode parentNode;
		private INode childNode;
		private INode goalNode;   // Only used for pathfinding
        private EdgeType edgeType;
		private bool bidirectional;

        public INode ParentNode
        {
            get { return parentNode; }
            set { parentNode = value; }
        }

        public INode ChildNode
        {
            get { return childNode; }
            set { childNode = value; }
        }

        public INode GoalNode
        {
            get { return goalNode; }
            set { goalNode = value; }
        }

        public EdgeType EdgeType
        {
            get { return edgeType; }
            set { edgeType = value; }
        }

        public bool Bidirectional
        {
            get { return bidirectional; }
            set { bidirectional = value; }
        }

		public float Distance {
			get {
				return parentNode.GetDistanceTo(childNode);
			}
		}

		public float Cost {
			get {
				return Distance + childNode.GetDistanceTo(goalNode);
			}
		}

		public Edge(INode parentNode, INode childNode, bool bidirectional, EdgeType edgeType)
		{
			this.parentNode = parentNode;
			this.childNode = childNode;
			this.bidirectional = bidirectional;
            this.edgeType = edgeType;
		}

		public Edge(INode parentNode, INode childNode, INode goalNode, EdgeType edgeType)
		{
			this.parentNode = parentNode;
			this.childNode = childNode;
			this.goalNode = goalNode;
            this.edgeType = edgeType;
		}
	}
}

