﻿using Kittan.AI.Enums;
using Kittan.AI.Pathfinding.Abstract;

namespace Kittan.AI.Pathfinding.Concrete
{
	public class Node : INode
	{
		private float x;
		private float y;
		private float distance;
        private NodeType nodeType;

		public float X {
			get { return x; }
			set { x = value; }
		}

		public float Y {
			get { return y; }
			set { y = value; }
		}

		public float Distance {
			get { return distance; }
			set { distance = value; }
		}

        public NodeType NodeType
        {
            get { return nodeType; }
            set { nodeType = value; }
        }

		public Node(int x, int y, NodeType nodeType)
		{
			this.x = x;
			this.y = y;
            this.nodeType = nodeType;
		}
	}
}

