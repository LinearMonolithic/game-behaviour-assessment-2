﻿using System.Collections.Generic;
using System.Linq;

using Kittan.AI.Enums;
using Kittan.AI.Pathfinding.Abstract;
using Kittan.AI.Pathfinding.Extensions;

using Microsoft.Xna.Framework;

namespace Kittan.AI.Pathfinding.Concrete
{
	public class Graph : IGraph
	{
		private List<INode> nodes;
		private List<IEdge> edges;

        public List<INode> Nodes
        {
            get { return nodes; }
        }

        public List<IEdge> Edges
        {
            get { return edges; }
        }

		public Graph()
		{
			nodes = new List<INode>();
			edges = new List<IEdge>();
		}

		public void AddNode(int x, int y, NodeType nodeType)
		{
			nodes.Add(new Node(x, y, nodeType));
		}

		public void RemoveNode(INode node)
		{
			var edgesToRemove = edges.Where((e) => e.ParentNode == node || e.ChildNode == node);
			edges = edges.Except(edgesToRemove).ToList();
			nodes.Remove(node);
		}

		public void AddEdge(INode parentNode, INode childNode, bool biDirectional, EdgeType edgeType)
		{
			edges.Add(new Edge(parentNode, childNode, biDirectional, edgeType));
		}

		public void RemoveEdge(IEdge edge)
		{
			edges.Remove(edge);
		}

		public List<INode> GetChildren(INode parentNode)
		{
			var returnList = edges
				.Where((e) => e.ParentNode == parentNode)
				.Select((e) => e.ChildNode);

			return returnList.ToList();
		}

		public List<IEdge> GetEdges(INode parentNode)
		{
			var returnList = edges.Where((e) => e.ParentNode == parentNode || (e.ChildNode == parentNode && e.Bidirectional == true));

			return returnList.ToList();
		}

		public float GetHeuristic(INode currentNode, INode goalNode)
		{
			return currentNode.GetDistanceTo(goalNode);
		}

		public List<IEdge> FindPath(INode startNode, INode goalNode)
		{
			var OpenList = new List<IEdge>();
			var ClosedList = new List<IEdge>();
			var returnPath = new List<IEdge>();

			var currentNode = startNode;

			while (currentNode != goalNode) {
				// Find the edges for the open nodes and cast them to be one directional, parent to child,
				// to make later operations easier.
				var openEdges = edges
					.Where((e) => e.ParentNode == currentNode || (e.ChildNode == currentNode && e.Bidirectional == true))
					.Select((e) => new Edge(
						currentNode,
						e.ParentNode == currentNode ? e.ChildNode : e.ParentNode,
						goalNode,
                        e.EdgeType))
					.ToList();

				foreach (var newEdge in openEdges) {
					var existingEdge = OpenList.SingleOrDefault((ol) => ol.ChildNode == newEdge.ChildNode);
					if (existingEdge != null) {
						if (existingEdge.Cost > newEdge.Cost) {
							OpenList.Remove(existingEdge);
							OpenList.Add(newEdge);
						}
					} else if (!ClosedList.Any((cls) => cls.ChildNode == newEdge.ChildNode)) {
						OpenList.Add(newEdge);
					}
				}

				// Sort the open list according to weighting
				OpenList = OpenList.OrderBy((e) => e.Cost).ToList();

				// Choose the one with the lowest weighting to open
				var edgeToConsider = OpenList.First();

				OpenList.Remove(edgeToConsider);
				ClosedList.Add(edgeToConsider);

				currentNode = edgeToConsider.ChildNode;
			}

			currentNode = goalNode;

			while (currentNode != startNode) {
				var edgeToAdd = ClosedList.Single((e) => e.ChildNode == currentNode);
				returnPath.Add(edgeToAdd);
				currentNode = edgeToAdd.ParentNode;
			}

			returnPath.Reverse();   // Reverse it, since it was populated backwards
			return returnPath;
		}

        public Node GetClosestNodeToPoint(Vector2 point)
        {
            var pointAsNode = new Node((int)point.X, (int)point.Y, NodeType.Node);
            Node closestNode = null;
            float closestNodeDistance = 0;

            foreach(var currentNode in nodes)
            {
                var currentNodeDistance = currentNode.GetDistanceTo(pointAsNode);
                if (closestNode == null || currentNodeDistance < closestNodeDistance)
                {
                    closestNode = currentNode as Node;
                    closestNodeDistance = currentNodeDistance;
                }
            }

            return closestNode;
        }

        // Create and return a node without adding it to the graph
        public Node CreateTemporaryNode(Vector2 point)
        {
            return new Node((int)point.X, (int)point.Y, NodeType.Node);
        }
    }
}

