﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kittan.AI.Enums
{
    public enum EdgeType
    {
        Walk,
        Jump,
        WaitForPlatform,
        Wait
    }
}
