﻿using Kittan.AI.Pathfinding.Concrete;

using Autofac;

namespace Kittan.AI.Autofac
{
    public class AIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Graph>().AsImplementedInterfaces();
        }
    }
}
