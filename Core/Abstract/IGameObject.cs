﻿using Microsoft.Xna.Framework;

namespace Kittan.Core.Abstract
{
	public interface IGameObject
	{
		void Update(GameTime gameTime);
		void Render();
		void Destroy();
	}
}
