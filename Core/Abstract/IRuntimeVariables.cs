﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Kittan.Core.Abstract
{
    public interface IRuntimeVariables
    {
        // General game related variables
        bool GameComplete { get; set; }

        // Debugging related variables
        bool DebugModeEnabled { get; set; }
        Texture2D DebugTexture { get; }

        // Physics related variables
        float GravityAcceleration { get; set; }
        float TimeDilation { get; set; }
        float BoundingBoxPadding { get; set; }
        int MaximumRayDistance { get; set; }

        // Player info
        Vector2? PlayerPosition { get; set; }
        Vector2 PlayerRespawnLocation { get; set; }

        // NPC related variables
        float MaxTimeBetweenNodes { get; set; }
    }
}
