﻿using System;

using Kittan.Core.Abstract;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Kittan.Core.Concrete
{
    /// <summary>
    /// This class is used to allow for shared core variables to be used in the system. An
    /// example would be the acceleration due to gravity, or if the program is in debug
    /// mode or not. This allows for a variable to be changed in one place yet seen by
    /// all code that uses it.
    /// </summary>
    public class RuntimeVariables : IRuntimeVariables
    {
        private bool gameComplete;
        private bool debugModeEnabled;
        private Texture2D debugTexture;
        private float gravityAcceleration;
        private float timeDilation;
        private float boundingBoxPadding;
        private int maximumRayDistance;
        private Vector2? playerPosition;
        private Vector2 playerRespawnLocation;
        private float maxTimeBetweenNodes;

        public bool GameComplete
        {
            get { return gameComplete; }
            set { gameComplete = value; }
        }

        public bool DebugModeEnabled
        {
            get { return debugModeEnabled; }
            set { debugModeEnabled = value; }
        }

        public Texture2D DebugTexture
        {
            get { return debugTexture; }
        }

        public float GravityAcceleration
        {
            get { return gravityAcceleration; }
            set { gravityAcceleration = value; }
        }

        public float TimeDilation
        {
            get { return timeDilation; }
            set { timeDilation = value; }
        }

        public float BoundingBoxPadding
        {
            get { return boundingBoxPadding;}
            set { boundingBoxPadding = value; }
        }

        public int MaximumRayDistance
        {
            get { return maximumRayDistance; }
            set { maximumRayDistance = value; }
        }

        public Vector2? PlayerPosition
        {
            get { return playerPosition; }
            set { playerPosition = value; }
        }

        public Vector2 PlayerRespawnLocation
        {
            get { return playerRespawnLocation; }
            set { playerRespawnLocation = value; }
        }

        public float MaxTimeBetweenNodes
        {
            get { return maxTimeBetweenNodes; }
            set { maxTimeBetweenNodes = value; }
        }

        public RuntimeVariables(GraphicsDevice graphicsDevice)
        {
            // Initialise default values
            debugModeEnabled = false;
            gravityAcceleration = 9.8f;
            timeDilation = 1.0f;
            boundingBoxPadding = 0;
            maximumRayDistance = 100;
            playerPosition = null;
            maxTimeBetweenNodes = 3000;

            // Initialise the debug texture
            debugTexture = new Texture2D(graphicsDevice, 1, 1, false, SurfaceFormat.Color);
            Int32[] pixel = { 0xFFFFFF };
            debugTexture.SetData<Int32>(pixel, 0, debugTexture.Width * debugTexture.Height);
        }
    }
}
