﻿using Kittan.Core.Concrete;

using Autofac;

namespace Kittan.Core
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RuntimeVariables>().AsImplementedInterfaces().SingleInstance();
        }
    }
}
